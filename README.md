# INTEGRATION MANUAL

 **Шaги:** 
 
 - [Package integration](#markdown-header-package-integration)
 
 - [Module intergration](#markdown-header-module)

### Up to Unity 2020
1. Перед импортом новой версии пакета необходимо удалить все, кроме AndroidManifest.xml, 
из папки Assets/Plugins/Android

2. Для Android: выполнить Assets -> External Dependency Manager -> Android Resolver -> Force resolve

3. Для Android: Project Settings -> Publishing Settings -> Minify установить флаги:
	- ✅ Release
	- ✅ Debug

### Package integration
Для того, чтобы внедрить пакет Vaveda Publishing в проект, необходимо сделать следующее:

 0) Перед тем, как устанавливать пакет, нужно на рабочем девайсе установить ключ ssh

 1) Внести в файл manifest.json информацию об источнике пакетов. Наш пакет имеет зависимости с Firebase, для этого нужно добавить *scopedRegistries*.  Вставить в зависимости следующую строку:
 

```json
	{
	  "dependencies": {
	    "games.vaveda.publishing": "ssh://git@bitbucket.org/vaveda/games.vaveda.publishing.git#0.6.4",
	    .....
	  },
	  "scopedRegistries": [
	      {
	        "name": "Game Package Registry by Google",
	        "url": "https://unityregistry-pa.googleapis.com",
	        "scopes": [
	          "com.google"
	        ]
	      }
	    ]
	}
```


 2) Для конфигурации Facebook следует выбрать в меню Facebook/Edit Settings. В появившемся меню инспектора ввести Facebook App Id ([fads-config\games.vaveda.YourProject\Configs\FacebookAppId](https://bitbucket.org/vaveda/fads-config/)), снять две гаочки в выпадающем меню App Events Settings и **нажать кнопку Regenerate Android Manifest для платформы андроид**.
 
 3) Скопировать папку Configs в Assets вашего проекта из [fads-config\games.vaveda.YourProject](https://bitbucket.org/vaveda/fads-config/)
 
 4) В проекте в настройках PlayerSettings->Player->Android->PublishingSettings->Build добавить флаги:
 
	 - ✅ Custom Main Manifest
	 - ✅ Custom Main Gradle Template
	 - ✅ Custom Launcher Gradle Template
	 - ✅ Custom Gradle Property template
 
  
 5) Сформированный файл Plugins/Android/launcherTemplate должен содержать следующую строку:

```JavaScript
	defaultConfig {
	        ......
	        
	        multiDexEnabled true
	}
```
 6) Сформированный файл Plugins/Android/gradleTemplate должен выглядеть как:
 
```JavaScript
	org.gradle.jvmargs=-Xmx**JVM_HEAP_SIZE**M
	org.gradle.parallel=true

	android.useAndroidX = true
	android.enableJetifier = true

	**ADDITIONAL_PROPERTIES**
```

7) Android:
   - Добавить поддержку OpenGLES2
   - Scripting Backends установить IL2CPP
   - Включить ARMv7 и ARM64 

## Module Integration

 1. Подписаться на event `OnInitializationComplete`
 2. Вызвать метод `Services.Instance.Initialize()` **Перед инициализацией чего-либо!!!**
 3. **После того как отработает event инициализировать или загружать все остальное**

``` csharp
//Choose the analytics what need for your project
    var analyticsBuilder = new AnalyticsBuilder()
                    .Add<FirebaseService>()
                    .Add<FacebookService>()
                    .Add<VavedaAnalyticsService>()
                    .Add<AdjustAnalyticsService>();
					
//Choose the ads what need for your project
    var adMask = AdShowType.Interstitial | AdShowType.Rewarded;

//For Android builds need listen Back Button
    Services.Instance.OnBackPressed += OnBackPressed;
	
//Example #1
	Services.Instance.OnInitializationComplete += OnInitializeComplete; 
  	Services.Instance.Initialize(analyticsBuilder, adMask);
	
//wait event...
	
//Example #2
	await Services.Instance.InitializeAsync(analyticsBuilder, adMask);
//Do other things
	
```

### Initialization:

``` csharp
//Show Interstitial after concrete level, default = 0  
	serviceInstance.FadsService.SetFirstLevelInterstitial(settings.FirstAdLevel);  
  
//How often call interstitial, default = 0
	serviceInstance.FadsService.SetLevelCounterForInterstitial(settings.AdLevelCounter);
```

### Analytic: 

``` csharp
//Call on level end, levelTime in seconds
//⚠️Level index from 1⚠️
//Level result: 0 - lose, 1 - win
//levelScore and totalScore default is 0
	
    serviceInstance.AnalyticsService.LevelEndEvent(levelName: "Level_11", levelIndex: 12, levelTime: 44, levelResult: 1, levelScore: 0,
            totalScore: 0);
```	

### Ads 
	
Banner:
	
``` csharp
//Show Banner 
//⚠️Only on level screen⚠️
	serviceInstance.FadsService.ShowBanner(Placements.PLACEMENT_BANNER_LEVEL);
	
//Hide Banner
	serviceInstance.FadsService.HideBanner();
```
Interstitial:

``` csharp
//Before ad update counters
	serviceInstance.FadsService.IncrementCounter();
	
//Set current level index for correct show interstitial
	serviceInstance.FadsService.SetLevelIndex(CurrentLevel);
	
	await serviceInstance.FadsService.ShowInterstitial(Placements.PLACEMENT_INTERSTITIAL_LEVEL_END);	
```
	
Rewarded Video:
	
``` csharp	
//Event for load video
	serviceInstance.FadsService.RewardedVideoLoaded += OnRewardedVideoLoaded;
	
//Example video #1
//Show video with result
	var isComplete = await serviceInstance.FadsService.ShowRewardedVideoAsync(Placements.PLACEMENT_REWARDED_LEVEL_XPOINTS);
	
//Example video #2
	if	(serviceInstance.FadsService.HasRewardedVideo)
	{
		serviceInstance.FadsService.RewardedVideoComplete += OnRewardedVideoComplete;
		serviceInstance.FadsService.RewardedVideoIncomplete += OnRewardedVideoIncomplete;
		serviceInstance.FadsService.ShowRewardedVideo(Placements.PLACEMENT_REWARDED_LEVEL_XPOINTS);
	}
```

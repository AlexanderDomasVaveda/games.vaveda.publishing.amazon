#ifndef FATT_hpp
#define FATT_hpp

#define	FATT_IS_UNITY	0

#if __cplusplus
#define FATTExtern	extern "C"
#else
#define FATTExtern	extern
#endif

/*
 -1 : not available
 
 typedef NS_ENUM(NSUInteger, ATTrackingManagerAuthorizationStatus) {
	 ATTrackingManagerAuthorizationStatusNotDetermined = 0,
	 ATTrackingManagerAuthorizationStatusRestricted,
	 ATTrackingManagerAuthorizationStatusDenied,
	 ATTrackingManagerAuthorizationStatusAuthorized
 } NS_SWIFT_NAME(ATTrackingManager.AuthorizationStatus) API_AVAILABLE(ios(14), macosx(11.0), tvos(14));
 */

FATTExtern int FATT_AuthorizationStatus(void);

FATTExtern bool FATT_ShouldShow(void);
FATTExtern void FATT_Show(void);
FATTExtern bool FATT_IsAvailableInSettings(void);
FATTExtern void FATT_RegisterApp(void);
FATTExtern void FATT_ConversionEvent( int event );
FATTExtern void FATT_OpenAppSettings(void);

#endif /* FATT_hpp */

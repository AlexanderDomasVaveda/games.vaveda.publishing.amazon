﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.IO;
using System.Linq;
using System.Threading.Tasks;
using UnityEditor;
using UnityEditor.PackageManager;
using UnityEngine;
using UnityEngine.Rendering;
using Vaveda.Integration.Scripts.Utils;
using Vaveda.Integration.Sources.Build.AndroidTarget;

namespace Vaveda.Integration.Sources
{
    public static class InitializationPackage
    {
        private const int Timeout = 100;

        [InitializeOnLoadMethod]
        private static async void InitializeOnLoad()
        {
            if (Defines.IsLogEnabled || Defines.IsUnityCloud)
            {
                Debug.Log($"[FAds Vaveda] Initialize publishing package");
            }

            var packagePath = await GetPackagePath();
            AmazonBuild.DeleteAndroidDirs();
            AmazonBuild.AddDefineSymbols();
            ConfigurePlayerSettings();

            if (string.IsNullOrEmpty(packagePath))
            {
                return;
            }

            CreateConfigFolder();
            CreateAndroidFiles(packagePath);
        }

        private static void ConfigurePlayerSettings()
        {
            if (Defines.IsLogEnabled || Defines.IsUnityCloud)
            {
                Debug.Log($"[FAds Vaveda] Configure PlayerSettings");
            }

            PlayerSettings.Android.targetSdkVersion = AndroidSdkVersions.AndroidApiLevelAuto;
            PlayerSettings.SetScriptingBackend(BuildTargetGroup.Android, ScriptingImplementation.IL2CPP);
            PlayerSettings.SetApiCompatibilityLevel(BuildTargetGroup.Android, ApiCompatibilityLevel.NET_4_6);
            PlayerSettings.Android.targetArchitectures = AndroidArchitecture.All;

            PlayerSettings.SetGraphicsAPIs(BuildTarget.Android,
                                           new[]
                                           {
                                               GraphicsDeviceType.Vulkan, GraphicsDeviceType.OpenGLES3,
                                               GraphicsDeviceType.OpenGLES2
                                           });

            PlayerSettings.SetApiCompatibilityLevel(BuildTargetGroup.iOS, ApiCompatibilityLevel.NET_4_6);
            PlayerSettings.iOS.targetOSVersionString = "11.0";
            PlayerSettings.SetArchitecture(BuildTargetGroup.iOS, 1);
        }

        private static async Task<string> GetPackagePath()
        {
            var listRequest = Client.List(true, true);

            while (!listRequest.IsCompleted)
            {
                await Task.Delay(Timeout);
            }

            var package = listRequest.Result.FirstOrDefault(x => x.name == Paths.PackageName);

            return null == package
                ? "Assets/games.vaveda.publishing/"
                : package.assetPath;
        }

        private static void CreateConfigFolder()
        {
            var configFolderPath = Path.Combine(Application.dataPath, Paths.Configs.ConfigsFolder);

            if (!Directory.Exists(configFolderPath))
            {
                Directory.CreateDirectory(configFolderPath);
            }

            if (Defines.IsAmazon)
            {
                return;
            }

            var srcFolder = Path.Combine(Paths.Configs.Packages, Paths.PackageName);
            var destFolder = Path.Combine(Paths.Configs.AssetsFolder);
            var srcPath = Path.Combine(srcFolder, Paths.Files.Linker);
            var destPath = Path.Combine(destFolder, Paths.Files.Linker);

            if (null == AssetDatabase.LoadAssetAtPath<TextAsset>(destPath))
            {
                AssetDatabase.CopyAsset(srcPath, destPath);
            }
        }

        private static void CreateAndroidFiles(string packagePath)
        {
            var androidFilesPath = Path.Combine(packagePath, "Editor", "AndroidDefault");
            var destPath = Path.Combine(Application.dataPath, "Plugins", "Android");

            if (!Directory.Exists(destPath))
            {
                Directory.CreateDirectory(destPath);
            }

            var files = Directory.GetFiles(androidFilesPath).Where(name => !name.EndsWith(".meta"));

            foreach (string file in files)
            {
                var destFile = Path.Combine(destPath, Path.GetFileName(file));

                if (File.Exists(destFile))
                {
                    if (Defines.IsLogEnabled || Defines.IsUnityCloud)
                    {
                        Debug.Log($"[FAds Vaveda] File exist: {Path.GetFileName(destFile)}");
                    }

                    continue;
                }

                if (Defines.IsLogEnabled || Defines.IsUnityCloud)
                {
                    Debug.Log($"[FAds Vaveda] Copy file: {Path.GetFileName(destFile)}");
                }

                File.Copy(file, destFile);
            }
        }
    }
}
﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEditor.Callbacks;
using UnityEngine;
using Vaveda.Integration.Scripts.Utils;
using Vaveda.Integration.Sources.Build.AndroidTarget;
using Vaveda.Integration.Sources.Build.IosTarget;

namespace Vaveda.Integration.Sources.Build
{
    public sealed class BuildFactory : IPreprocessBuildWithReport
    {
        private static BuildSettings settings;

        public int callbackOrder => 1;

        public void OnPreprocessBuild(BuildReport report)
        {
            settings = new BuildSettings(report);

            if ((Defines.IsUnityCloud) && settings.Publishing == Publishing.Release)
            {
                var defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(report.summary.platformGroup);

                PlayerSettings.SetScriptingDefineSymbolsForGroup(report.summary.platformGroup,
                    "UNITY_CLOUD_MASTER;" + defines);
            }

            var builds = new List<IPreBuild> {GetBuildByTarget<IPreBuild>(settings.Platform)};

            foreach (var build in builds)
            {
                build.PreBuild(settings);
            }
        }

        [PostProcessBuild(100)]
        private static void OnPostprocessBuild(BuildTarget target, string path)
        {
            try
            {
                var builds = new List<IPostBuild> {GetBuildByTarget<IPostBuild>(settings.Platform)};

                foreach (var build in builds)
                {
                    build.PostBuild(settings);
                }

                Debug.Log($"[FAds Vaveda] {settings.Publishing} build complete");
            }
            catch (BuildFailedException ex)
            {
                Debug.LogError($"[FAds Vaveda] {ex.Message}");
            }
        }

        private static T GetBuildByTarget<T>(Platforms platform) where T : IBuild
        {
            switch (platform)
            {
                case Platforms.Amazon:
                    return (T)(new AmazonBuild() as IBuild);
                case Platforms.Android:
                    return (T)(new AndroidBuild() as IBuild);
                case Platforms.Ios:
                    return (T)(new IosBuild() as IBuild);
                default:
                    Debug.Log("[Build Factory] Not supported platform");
                    throw new InvalidOperationException();
            }
        }
    }
}
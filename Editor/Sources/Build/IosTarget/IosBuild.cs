﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Callbacks;
using UnityEngine;

namespace Vaveda.Integration.Sources.Build.IosTarget
{
    public class IosBuild : IPreBuild, IPostBuild
    {
        public void PreBuild(BuildSettings settings)
        {
            CopyConfigFiles(settings.MovableFilePaths.Where(movableFile => !movableFile.IsPostBuildOnly).ToList());
        }

        public void PostBuild(BuildSettings settings)
        {
            CopyConfigFiles(settings.MovableFilePaths.Where(movableFile => movableFile.IsPostBuildOnly).ToList());
            //TODO: move to this class
            IosXprojBuildPostprocess.OnPostprocessBuild(settings);

            AddPauseOnAd(settings.Report.summary.outputPath);

            DeleteConfigFiles(settings.MovableFilePaths);
        }

        private static void CopyConfigFiles(IEnumerable<BuildSettings.MovableFile> files)
        {
            foreach (var file in files)
            {
                var fileText = File.ReadAllText(file.SourcePath);
                var isNullOrEmpty = string.IsNullOrEmpty(fileText);

                switch (isNullOrEmpty)
                {
                    case true when file.IsImportant:
                        throw
                            new BuildFailedException($"File '{file.SourcePath}' doesn't exist");

                    case true when !file.IsImportant:
                        Debug.LogWarning($"[FAds Vaveda] Error: File '{file.SourcePath}' doesn't exist. It is optional");
                        continue;

                    default:
                        File.WriteAllText(file.DestinationPath, fileText);
                        break;
                }
            }
        }

        private static void DeleteConfigFiles(IEnumerable<BuildSettings.MovableFile> movableFiles)
        {
            foreach (var file in movableFiles)
            {
                if (file.IsPostBuildOnly)
                {
                    continue;
                }

                Debug.Log($"[FAds Vaveda]: Delete file '{file.DestinationPath}'");

                AssetDatabase.DeleteAsset(file.DestinationPath);
            }
        }

        /// <summary>
        /// Add fix for all ads, pause unity after start ad
        /// </summary>
        /// <param name="buildPath">Path to build folder</param>
        /// <exception cref="BuildFailedException">Don't contains some things</exception>
        private static void AddPauseOnAd(string buildPath)
        {
            const string scriptName = "UnityAppController+Rendering.mm";

            var scriptPath = Directory.GetFiles(buildPath, scriptName, SearchOption.AllDirectories).FirstOrDefault();

            if (string.IsNullOrEmpty(scriptPath))
            {
                throw new BuildFailedException($"{scriptName} file not found");
            }

            const string beforeLine = "if (!UnityIsPaused())";
            const string afterLine = "if (!UnityIsPaused() && !FAdsShouldPauseRender())";

            var scriptText = File.ReadAllText(scriptPath);

            scriptText = scriptText.Insert(0, "extern \"C\" bool FAdsShouldPauseRender();\n");
            scriptText = scriptText.Replace(beforeLine, afterLine);

            if (!scriptText.Contains(afterLine))
            {
                throw new BuildFailedException($"{afterLine} not replaced");
            }

            File.WriteAllText(scriptPath, scriptText);
        }

        [PostProcessBuild(49)]
        private static void OnPostprocessBuild(BuildTarget target, string path)
        {
            var podfile = Path.Combine(path, "Podfile");
            if (!File.Exists(podfile))
            {
                return;
            }

            Debug.Log($"[FAds Vaveda] remove source and use_frameworks from {podfile}");

            File.WriteAllLines(podfile,
                               File.ReadLines(podfile)
                                   .Where(l =>
                                              (l != "source \'https://github.com/CocoaPods/Specs.git\'"
                                               && l != "use_frameworks!"))
                                   .ToList());
            
            Debug.Log($"[FAds Vaveda] Podfile:\n{File.ReadAllText(podfile)}");  
        }
    }
}
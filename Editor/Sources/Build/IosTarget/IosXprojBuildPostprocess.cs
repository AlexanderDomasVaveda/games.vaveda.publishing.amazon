// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;
using UnityEditor.iOS.Xcode;
using Debug = UnityEngine.Debug;

namespace Vaveda.Integration.Sources.Build.IosTarget
{
    public static class IosXprojBuildPostprocess
    {
        private static string BuildFrameworkPath;

        public static void OnPostprocessBuild(BuildSettings settings)
        {
            var buildPath = settings.Report.summary.outputPath;

            Debug.Log("[FAds Vaveda] Adding Framework Search Paths to Xcode project");

            var rootPath = Path.Combine("Assets", "games.vaveda.publishing", "Editor");
            if (!Directory.Exists(rootPath))
            {
                rootPath = Path.Combine("Packages", "games.vaveda.publishing", "Editor");
            }

            BuildFrameworkPath = Path.Combine(rootPath, "buildframework");

            var projPath = Path.Combine(buildPath, "Unity-iPhone.xcodeproj/project.pbxproj");
            var project = new PBXProject();
            project.ReadFromString(File.ReadAllText(projPath));

            ExecuteCommand($"ls {Path.Combine(buildPath, "Pods/")}");

            InitializeFrameworks(project, buildPath);

            PreparePlist(settings);

            File.WriteAllText(projPath, project.WriteToString());

            Debug.Log("[FAds Vaveda] OnPostprocessBuild Done");
        }

        private static void InitializeFrameworks(PBXProject project, string buildPath)
        {
            var targetUnityIPhone = project.GetUnityMainTargetGuid();
            var targetUnityFramework = project.GetUnityFrameworkTargetGuid();

            project.AddBuildProperty(targetUnityIPhone, "CLANG_ENABLE_MODULES", "YES");
            project.AddBuildProperty(targetUnityIPhone, "ENABLE_BITCODE", "NO");
            
            project.AddBuildProperty(targetUnityFramework, "CLANG_ENABLE_MODULES", "YES");
            project.AddBuildProperty(targetUnityFramework, "ENABLE_BITCODE", "NO");
            
            //TODO Try remove me
            project.SetBuildProperty(targetUnityFramework, "SWIFT_VERSION", "5.0");

            // Force Set ARCHS
            project.SetBuildProperty(targetUnityIPhone, "ARCHS", "arm64");
            project.SetBuildProperty(targetUnityFramework, "ARCHS", "arm64");

            var configGuid = project.AddFile("FAdsConfig", "FAdsConfig");
            project.AddFileToBuild(targetUnityIPhone, configGuid);
            project.AddFileToBuild(targetUnityFramework, configGuid);
            
            var swiftGuid = project.AddFile("EmptyFile.swift", "EmptyFile.swift");
            project.AddFileToBuild(targetUnityIPhone, swiftGuid);
            project.AddFileToBuild(targetUnityFramework, swiftGuid);
            
            var fattGuid = project.AddFile("FATT.hpp", "FATT.hpp");
            project.AddFileToBuild(targetUnityIPhone, fattGuid);
            project.AddFileToBuild(targetUnityFramework, fattGuid);

            var googleServiceInfo = project.AddFile("GoogleService-Info.plist", "GoogleService-Info.plist");
            project.AddFileToBuild(targetUnityIPhone, googleServiceInfo);
        }

        private static void ExecuteCommand(string command)
        {
            Debug.Log($"[FAds Vaveda] execute command:\n\t'{command}'");
            Process proc = new Process
            {
                StartInfo =
                {
                    FileName = "/bin/zsh",
                    Arguments = "-c ' " + command + " '",
                    UseShellExecute = false,
                    RedirectStandardOutput = true
                }
            };
            proc.Start();
            string output = proc.StandardOutput.ReadToEnd();
            Debug.Log($"[FAds Vaveda] command output:\n\t====start===\n{output}\n\t====end===");
            proc.WaitForExit();
        }

        private static void PreparePlist(BuildSettings settings)
        {
            string plistPath = Path.Combine(settings.BuildPath, "Info.plist");
            PlistDocument plist = new PlistDocument();
            plist.ReadFromString(File.ReadAllText(plistPath));
            // Get root
            PlistElementDict rootDict = plist.root;
          
            rootDict.SetString("NSUserTrackingUsageDescription", "This identifier will be used to deliver personalized ads to you.");
            
            //applivin key
            var applovinSdkKey = settings.ApplovinKey;
            rootDict.SetString("AppLovinSdkKey", applovinSdkKey);

            //admob key
            var googleSdkKey = settings.GoogleSdkKey;
            rootDict.SetString("GADApplicationIdentifier", googleSdkKey);

            rootDict.SetBoolean("GADIsAdManagerApp", true);
            rootDict.SetBoolean("FIREBASE_ANALYTICS_COLLECTION_ENABLED", false);
            rootDict.SetBoolean("FirebaseMessagingAutoInitEnabled", false);
            rootDict.SetBoolean("FirebaseAutomaticScreenReportingEnabled", false);

            //deprecated key in ios13
            rootDict.values.Remove("UIApplicationExitsOnSuspend");

            // AddTransportFlags(rootDict);
            AddPrivacyDescription(rootDict);

            // Disable Facebook auto log app events and advertiser ID collection  
            rootDict.SetBoolean("FacebookAutoLogAppEventsEnabled", false);
            rootDict.SetBoolean("FacebookAdvertiserIDCollectionEnabled", false);

            // Write to file
            File.WriteAllText(plistPath, plist.WriteToString());

            AddSKAdNetworkItems(plistPath);
        }

        private static void AddSKAdNetworkItems(string plistPath)
        {
            var textPlist = File.ReadAllText(plistPath);
            var textSkAd = File.ReadAllText(Path.Combine(BuildFrameworkPath, "SKAdNetworkItems-merged.txt"));
            const string dict = "<dict>";
            textPlist = new Regex(Regex.Escape(dict)).Replace(textPlist, dict + "\n" + textSkAd, 1);
            File.WriteAllText(plistPath, textPlist);

            Debug.Log($"[FAds Vaveda] SKAdNetworkItems added");
        }

        private static void AddPrivacyDescription(PlistElementDict rootDict)
        {
            if (!rootDict.values.ContainsKey("NSPhotoLibraryUsageDescription"))
            {
                rootDict.values.Add("NSPhotoLibraryUsageDescription",
                    new PlistElementString("For taking pictures"));
            }

            if (!rootDict.values.ContainsKey("NSCalendarsUsageDescription"))
            {
                rootDict.values.Add("NSCalendarsUsageDescription",
                    new PlistElementString("For calendar events creation"));
            }
        }
    }
}
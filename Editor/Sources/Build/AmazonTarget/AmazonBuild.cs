﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;

namespace Vaveda.Integration.Sources.Build.AndroidTarget
{
    public class AmazonBuild : IPreBuild, IPostBuild
    {
        public void PreBuild(BuildSettings settings)
        {
            CopyFadsManifest(settings);
            CopyConfigFiles(settings.MovableFilePaths);
        }

        public void PostBuild(BuildSettings settings)
        {
            DeleteConfigFiles(settings.MovableFilePaths);
        }

        private static void CopyConfigFiles(IEnumerable<BuildSettings.MovableFile> movableFiles)
        {
            foreach (var file in movableFiles)
            {
                var fullPath = Path.GetDirectoryName(Path.Combine(Environment.CurrentDirectory, file.DestinationPath));

                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath ?? string.Empty);
                }

                var result = AssetDatabase.CopyAsset(file.SourcePath, file.DestinationPath);

                if (!result && file.IsImportant)
                {
                    throw new BuildFailedException($"[FAds Vaveda] Copy file [{file.SourcePath}] failed");
                }
            }
        }

        private static void DeleteConfigFiles(IEnumerable<BuildSettings.MovableFile> movableFiles)
        {
            foreach (var file in movableFiles)
            {
                var result = AssetDatabase.DeleteAsset(file.DestinationPath);

                Debug.Log($"[FAds Vaveda]: Delete file '{file.SourcePath}'");

                if (!result && file.IsImportant)
                {
                    throw new BuildFailedException($"[FAds Vaveda] Delete file [{file.SourcePath}] failed");
                }
            }
        }

        private static void CopyFadsManifest(BuildSettings settings)
        {
        }

        public static void DeleteAndroidDirs()
        {
        }

        public static void AddDefineSymbols()
        {
        }
    }
}
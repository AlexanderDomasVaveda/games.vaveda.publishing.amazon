// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

namespace Vaveda.Integration.Sources.Build
{
    public interface IPostBuild : IBuild
    {
        void PostBuild(BuildSettings settings);
    }
}
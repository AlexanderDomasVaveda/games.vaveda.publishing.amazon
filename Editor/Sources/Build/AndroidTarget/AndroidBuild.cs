﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Build;
using UnityEngine;

namespace Vaveda.Integration.Sources.Build.AndroidTarget
{
    public class AndroidBuild : IPreBuild, IPostBuild
    {
        private const string AndroidManifest = "AndroidManifest.xml";
        private const string ProjectProperties = "project.properties";

        public void PreBuild(BuildSettings settings)
        {
            CopyFadsManifest(settings);
            CopyConfigFiles(settings.MovableFilePaths);
            FixFirebaseManifest();
            CheckFacebookManifest();
        }

        private void CheckFacebookManifest()
        {
            var manifestPath = Path.Combine(Application.dataPath, "Plugins", "Android", AndroidManifest);

            if (!File.Exists(manifestPath))
            {
                return;
            }

            var text = File.ReadAllText(manifestPath);

            text = Regex.Replace(text, "<meta-data android:name=\"com.facebook.sdk.AutoLogAppEventsEnabled\".*/>",
                                 "<meta-data android:name=\"com.facebook.sdk.AutoLogAppEventsEnabled\" android:value=\"false\" />");

            text = Regex.Replace(text, "<meta-data android:name=\"com.facebook.sdk.AdvertiserIDCollectionEnabled\".*/>",
                                 "<meta-data android:name=\"com.facebook.sdk.AdvertiserIDCollectionEnabled\" android:value=\"false\" />");

            File.WriteAllText(manifestPath, text);
        }

        private void FixFirebaseManifest()
        {
            var manifestPath = Path.Combine(Application.dataPath, "Plugins", "Android", "Firebase", AndroidManifest);

            if (!File.Exists(manifestPath))
            {
                return;
            }

            var text = Regex.Replace(File.ReadAllText(manifestPath), "(<uses-sdk.*minSdk.*14.*/>)", "");
            File.WriteAllText(manifestPath, text);
        }

        public void PostBuild(BuildSettings settings)
        {
            DeleteConfigFiles(settings.MovableFilePaths);
        }

        private static void CopyConfigFiles(IEnumerable<BuildSettings.MovableFile> movableFiles)
        {
            foreach (var file in movableFiles)
            {
                var fullPath = Path.GetDirectoryName(Path.Combine(Environment.CurrentDirectory, file.DestinationPath));

                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath ?? string.Empty);
                }

                var result = AssetDatabase.CopyAsset(file.SourcePath, file.DestinationPath);

                if (!result && file.IsImportant)
                {
                    throw new BuildFailedException($"[FAds Vaveda] Copy file [{file.SourcePath}] failed");
                }
            }
        }

        private static void DeleteConfigFiles(IEnumerable<BuildSettings.MovableFile> movableFiles)
        {
            foreach (var file in movableFiles)
            {
                var result = AssetDatabase.DeleteAsset(file.DestinationPath);
                Debug.Log($"[FAds Vaveda]: Delete file '{file.SourcePath}'");

                if (!result && file.IsImportant)
                {
                    throw new BuildFailedException($"[FAds Vaveda] Delete file [{file.SourcePath}] failed");
                }
            }
        }

        private static void CopyFadsManifest(BuildSettings settings)
        {
            var fAdsFolder = Path.Combine(Application.dataPath, "Plugins", "Android", "FAds.androidlib/");
            var manifestPath = Path.Combine(fAdsFolder, AndroidManifest);
            var projPropPath = Path.Combine(fAdsFolder, ProjectProperties);

            if (!Directory.Exists(fAdsFolder))
            {
                Directory.CreateDirectory(fAdsFolder);
            }

            var sb = new StringBuilder();
            sb.AppendLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
            sb.AppendLine("<manifest xmlns:android=\"http://schemas.android.com/apk/res/android\" package=\"com.fabros\">");
            sb.AppendLine("\t<application>");
            sb.AppendLine("\t\t<meta-data android:name=\"applovin.sdk.key\"");
            sb.AppendLine($"\t\t\tandroid:value=\"{settings.ApplovinKey}\"/>");
            sb.AppendLine("\t\t<meta-data android:name=\"com.google.android.gms.ads.APPLICATION_ID\"");
            sb.AppendLine($"\t\t\tandroid:value=\"{settings.GoogleSdkKey}\"/>");
            sb.AppendLine($"\t\t<meta-data android:name=\"firebase_analytics_collection_enabled\" android:value=\"false\" />");
            sb.AppendLine($"\t\t<meta-data android:name=\"google_analytics_automatic_screen_reporting_enabled\" android:value=\"false\" />");
            sb.AppendLine("\t</application>");
            sb.AppendLine("\t<uses-permission android:name=\"android.permission.VIBRATE\" />");
            sb.AppendLine("</manifest>");
            var manifest = sb.ToString();
            File.WriteAllText(manifestPath, manifest);

            if (!File.Exists(projPropPath))
            {
                File.WriteAllText(projPropPath, "android.library=true");
            }
        }
    }
}
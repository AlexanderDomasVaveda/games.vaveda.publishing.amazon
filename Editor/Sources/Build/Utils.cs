// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Diagnostics;
using Debug = UnityEngine.Debug;

namespace Vaveda.Integration.Sources.Build
{
    public static class Utils
    {
       
        public static void ExecuteCommand(string command)
        {
            Process proc = new Process
            {
                StartInfo =
                {
                    FileName = "/bin/zsh",
                    Arguments = "-c ' " + command + " '",
                    UseShellExecute = false,
                    RedirectStandardOutput = true
                }
            };
            proc.Start();
            string output = proc.StandardOutput.ReadToEnd();
            Debug.Log($"[FAds Vaveda] execute command: '{command}' -> output:\n====start===\n{output}\n====end===");
            proc.WaitForExit();
        }
    }
}
// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using Vaveda.Integration.Scripts.Utils;
using static Vaveda.Integration.Sources.Paths;

namespace Vaveda.Integration.Sources.Build
{
    public enum Platforms
    {
        Amazon,
        Android,
        Ios
    }

    public enum Publishing
    {
        Release,
        Test
    }

    public class BuildSettings
    {
        private static class Files
        {
            /// <summary>
            /// Name with formats {0} - <see cref="Platforms"/>, {1} - <see cref="Publishing"/>
            /// </summary>
            public const string FAdsConfigJson = "FAdsConfig{0}{1}.json";

            public const string FAdsConfig = "FAdsConfig";
            
            public const string EmptySwiftFile = "EmptyFile.swift";

            ///<inheritdoc cref="FAdsConfigJson"/>
            public const string GoogleSdkKey = "GoogleSdkKey{0}{1}.txt";

            /// <summary>
            /// Name with format {0} - <see cref="Publishing"/>
            /// </summary>
            public const string ApplovinSdkKey = "ApplovinKey{0}.txt";

            public const string AdjustSdkKey = "AdjustKey{0}Release.txt";

            public const string AdjustEvents = "AdjustEvents{0}Release.txt";

            public const string PublishingSdkVersion = "PublishingSdkVersion.txt";
        }

        public class MovableFile
        {
            public MovableFile(string sourcePath, string destinationPath, bool isImportant, bool postBuildOnly)
            {
                SourcePath = sourcePath;
                DestinationPath = destinationPath;
                IsImportant = isImportant;
                IsPostBuildOnly = postBuildOnly;

                if (Defines.IsLogEnabled || Defines.IsUnityCloud)
                {
                    Debug.Log($"[FAds Vaveda] MovableFile: {SourcePath}->{DestinationPath}");
                }
            }

            public string SourcePath { get; }
            public string DestinationPath { get; }
            public bool IsImportant { get; }
            public bool IsPostBuildOnly { get; }
        }

        public BuildReport Report { get; }

        public Platforms Platform { get; }
        public Publishing Publishing { get; }

        public string BuildPath { get; }
        public string ApplovinKey { get; }
        public string GoogleSdkKey { get; }

        public IReadOnlyList<MovableFile> MovableFilePaths { get; }

        public BuildSettings(BuildReport report)
        {
            AddPublishingVersion();

            Report = report;
            Platform = GetPlatformByBuildTarget(report.summary.platform);
            Publishing = GetPublishing();

            BuildPath = report.summary.outputPath;

            ApplovinKey = Defines.IsAmazon
                ? ""
                : GetKeyByName(string.Format(Files.ApplovinSdkKey, Publishing.ToString()), Publishing);
            
            GoogleSdkKey = GetKeyByName(string.Format(Files.GoogleSdkKey, Platform.ToString(), Publishing.ToString()),
                Publishing);

            MovableFilePaths = GetMovableFiles(Platform, Publishing, report.summary.outputPath).ToList();
        }

        private void AddPublishingVersion()
        {
            var version = "none";

            var linkerPath = Path.Combine(Configs.Packages, "packages-lock.json");
            if (File.Exists(linkerPath))
            {
                var text = File.ReadAllText(linkerPath);
                if (!String.IsNullOrEmpty(text))
                {
                    var pub = Regex.Match(text, @"(""ssh.*vaveda\.publishing.*git.*"")");
                    if (pub.Success)
                    {
                        var val = pub.Value.Replace("\"", "");
                        var split = val.Split('#');
                        version = split.Length == 2 ? split[1] : "master";
                    }
                }
            }

            if (Defines.IsLogEnabled || Defines.IsUnityCloud)
            {
                Debug.Log($"[FAds Vaveda] Publishing version: {version}");
            }

            string destPublishingSdkVersionPath =
                Path.Combine(Configs.AssetsFolder, Configs.ResourcesFolder, Files.PublishingSdkVersion);
            File.WriteAllText(destPublishingSdkVersionPath, version);
        }

        private static Platforms GetPlatformByBuildTarget(BuildTarget target)
        {
            switch (target)
            {
                case BuildTarget.Android:
                    return Defines.IsAmazon ? Platforms.Amazon : Platforms.Android;

                case BuildTarget.iOS:
                    return Platforms.Ios;

                default:
                    throw new BuildFailedException($"[FAds Vaveda] {target} doesn't support this target");
            }
        }

        /// <summary>
        /// Get branch from git
        /// </summary>
        /// <returns>Publishing type</returns>
        private static Publishing GetPublishing()
        {
            const string MasterBranchName = "ref: refs/heads/master";

            var headPath = Path.Combine(Environment.CurrentDirectory, ".git", "HEAD");

            if (!File.Exists(headPath))
            {
                return Publishing.Test;
            }

            var headText = File.ReadAllText(headPath).Trim();

            return headText.Contains(MasterBranchName) ? Publishing.Release : Publishing.Test;
        }

        private static string GetKeyByName(string fileName, Publishing publishing)
        {
            var rootFolder = GetRootFolderByPublishing(publishing);
            var filePath = Path.Combine(rootFolder, Configs.ConfigsFolder, fileName);

            var file = AssetDatabase.LoadAssetAtPath<TextAsset>(filePath);

            if (null == file)
            {
                throw new BuildFailedException($"[FAds Vaveda] {filePath} doesn't contains file");
            }

            return file.text.Trim();
        }

        private static IEnumerable<MovableFile> GetMovableFiles(Platforms platform, Publishing publishing,
            string outputPath)
        {
            var configPath = Path.Combine(Configs.AssetsFolder, Configs.ConfigsFolder);

            var srcConfigFileName = string.Format(Files.FAdsConfigJson, platform.ToString(), publishing.ToString());
            var srcConfigFolderPath = Path.Combine(GetRootFolderByPublishing(publishing), Configs.ConfigsFolder);
            var srcConfigPath = Path.Combine(srcConfigFolderPath, srcConfigFileName);

            var fileNameAdjustKey = string.Format(Files.AdjustSdkKey, platform.ToString());
            var srcPathAdjustKey = Path.Combine(configPath, fileNameAdjustKey);
            string destPathAdjustKey =
                Path.Combine(Configs.AssetsFolder, Configs.ResourcesFolder, "AdjustKeyRelease.txt");
            yield return new MovableFile(srcPathAdjustKey, destPathAdjustKey, false, false);

            var fileNameAdjustEvents = string.Format(Files.AdjustEvents, platform.ToString());
            var srcPathAdjustEvents = Path.Combine(configPath, fileNameAdjustEvents);
            string destPathAdjustEvents =
                Path.Combine(Configs.AssetsFolder, Configs.ResourcesFolder, "AdjustEventsRelease.txt");
            yield return new MovableFile(srcPathAdjustEvents, destPathAdjustEvents, false, false);

            string destConfigPath;
            switch (platform)
            {
                case Platforms.Amazon:
                    yield break;
                case Platforms.Android:
                    var streamingAssetsPath = Path.Combine(Configs.AssetsFolder, Configs.StreamingAssetsFolder);

                    destConfigPath = Path.Combine(streamingAssetsPath, Files.FAdsConfig);

                    yield return new MovableFile(srcConfigPath, destConfigPath,
                        true, false);
                    yield break;

                case Platforms.Ios:
                    destConfigPath = Path.Combine(outputPath, Files.FAdsConfig);

                    yield return new MovableFile(srcConfigPath, destConfigPath,
                                                 true, true);
                    
                    var rootPath = Path.Combine("Assets", "games.vaveda.publishing", "Editor");
                    if (!Directory.Exists(rootPath))
                    {
                        rootPath = Path.Combine("Packages", "games.vaveda.publishing", "Editor");
                    }
                    yield return new MovableFile(Path.Combine(rootPath, "buildframework", Files.EmptySwiftFile),
                                                 Path.Combine(outputPath, Files.EmptySwiftFile),
                                                 true, true);
                    yield return new MovableFile(Path.Combine(rootPath, "buildframework", "FATT.hpp"),
                                                 Path.Combine(outputPath, "FATT.hpp"),
                                                 true, true);
                    yield break;

                default:
                    throw new BuildFailedException($"[FAds Vaveda] {platform} doesn't support this platform");
            }
        }

        private static string GetRootFolderByPublishing(Publishing publishing) =>
            publishing == Publishing.Release ? Configs.AssetsFolder : Path.Combine(Configs.Packages, PackageName);
    }
}

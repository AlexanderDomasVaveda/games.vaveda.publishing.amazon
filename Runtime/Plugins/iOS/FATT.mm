
#include "FATT.hpp"

#import <AppTrackingTransparency/AppTrackingTransparency.h>
#import <UIKit/UIKit.h>
#import <StoreKit/StoreKit.h>

#if FATT_IS_UNITY
void UnitySendMessage(const char*, const char*, const char*);
#endif

@protocol FATTDelegate <NSObject>
@required
-(void) FATTDialogResult:(int) status;
@end

static __weak id<FATTDelegate> _gDelegate = nil;


int FATT_AuthorizationStatus() {
	if( @available(iOS 14.0, *) ) {
		return (int)ATTrackingManager.trackingAuthorizationStatus;
	} else {
		return -1;
	}
}

bool FATT_ShouldShow() {
	if( @available(iOS 14.0, *) ) { // 14.5 ?
		const ATTrackingManagerAuthorizationStatus trackingAuthorizationStatus = ATTrackingManager.trackingAuthorizationStatus;
		return trackingAuthorizationStatus == ATTrackingManagerAuthorizationStatusNotDetermined;
	}
	return false;
}

void FATT_Show() {
	if( @available(iOS 14.0, *) ) {
		[NSUserDefaults.standardUserDefaults setInteger:1 forKey:@"FATT_Show"];
		[ATTrackingManager requestTrackingAuthorizationWithCompletionHandler:^(ATTrackingManagerAuthorizationStatus status) {
#if FATT_IS_UNITY
		 UnitySendMessage( "", "", [NSString stringWithFormat:@"%d", (int)status].UTF8String );
#endif
		 [_gDelegate FATTDialogResult:(int)status];
		 }];
	}
}

bool FATT_IsAvailableInSettings() {
	if( @available(iOS 14.0, *) ) {
		const ATTrackingManagerAuthorizationStatus trackingAuthorizationStatus = ATTrackingManager.trackingAuthorizationStatus;
		const auto flag = [NSUserDefaults.standardUserDefaults integerForKey:@"FATT_Show"];
		return trackingAuthorizationStatus == ATTrackingManagerAuthorizationStatusDenied  &&  flag;
	}
	return false;
}

void FATT_RegisterApp() {
	if( @available(iOS 11.3, *) ) {
		[SKAdNetwork registerAppForAdNetworkAttribution];
	}
}

void FATT_ConversionEvent( int event ) {
	if( @available(iOS 14.0, *) ) {
		[SKAdNetwork updateConversionValue:event];
	}
}

void FATT_OpenAppSettings() {
	NSURL * url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
	[UIApplication.sharedApplication openURL:url options:@{} completionHandler:nil];
}


///////////
//

@interface FATT : NSObject
+(void) setDelegate:(id<FATTDelegate>) delegate;
+(int) AuthorizationStatus;
+(bool) ShouldShow;
+(void) Show;
+(bool) IsAvailableInSettings;
+(void) OpenAppSettings;
+(void) RegisterApp;
+(void) ConversionEvent:(int) event;
@end

@implementation FATT

+(void) setDelegate:(id<FATTDelegate>) delegate {
	_gDelegate = delegate;
}

+(int) AuthorizationStatus {
	return FATT_AuthorizationStatus();
}

+(bool) ShouldShow {
	return FATT_ShouldShow();
}

+(void) Show {
	FATT_Show();
}

+(bool) IsAvailableInSettings {
	return FATT_IsAvailableInSettings();
}

+(void) OpenAppSettings {
	return FATT_OpenAppSettings();
}

+(void) RegisterApp {
	FATT_RegisterApp();
}

+(void) ConversionEvent:(int) event {
	FATT_ConversionEvent( event );
}

@end


#import <Foundation/Foundation.h>
#import <FAds/FAds.h>
#include <string>

extern "C" {
void FAdsUnityInitialize(void);
void FAdsUnitySetBannerPosition( int enum_position, float bannerOffset);
}

@interface FAdsUnityWrapper : NSObject< FAdsDelegate >
+(instancetype) shared;
@end

#import "FAdsUnityWrapper.h"
#import <Foundation/Foundation.h>

static int _unityBannerPosition = 0;
static float _bannerOffset = 0.0;

class FAdsUtils {
public:
    static std::string activeConnection();
};

extern "C" {
    void FAdsUnityInitialize(void) {
        FAdsSetDelegate( FAdsUnityWrapper.shared );
    }
    
    void FAdsUnitySetBannerPosition( int enum_position, float bannerOffset ) {
        _unityBannerPosition = enum_position;
        _bannerOffset = bannerOffset;
    }
    
    const char * FAdsUnityGetConnection() {
        return strdup( FAdsUtils::activeConnection().c_str() );
    }

    bool isTabletAdLayout() {
        return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
    }
    
    const char *  FAdsUnityGetNativeVersion(){
        return strdup(FAdsVersion);
    }
}

@implementation FAdsUnityWrapper

NSString *const Separator = @"!@#";

static FAdsUnityWrapper * _shared = nil;

+(instancetype) shared {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _shared = [[FAdsUnityWrapper alloc] init];
    });
    return _shared;
}

+ (const char *) NSStringToChar:(NSString *)value {
    return [value UTF8String] ?: "";
}

- (NSString *) dictToJSONString:(NSDictionary *)dict {
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dict
                                                       options:0
                                                         error:&error];
    if (!jsonData) {
        NSLog(@"JSON error: %@", error);
        return nil;
    } else {
        NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
        return JSONString;
    }
}

-(void) FAdsBannerPosition:(UIView *) adView contentSizeWidth:(double) contentSizeWidth contentSizeHeight:(double) contentSizeHeight {
    UIView * appView = UIApplication.sharedApplication.keyWindow.rootViewController.view;
    const CGSize parentSize = appView.bounds.size;
    UIEdgeInsets safeAreaInsets = UIEdgeInsetsZero;
    if( @available(iOS 11.0, *) ) {
        safeAreaInsets = appView.safeAreaInsets;
    }
	
    adView.hidden = NO;
    float d = _bannerOffset * contentSizeHeight;;
    if( _unityBannerPosition == 1 ) {
		// top
        adView.frame = CGRectMake( ( parentSize.width - contentSizeWidth ) * 0.5, safeAreaInsets.top + d, contentSizeWidth, contentSizeHeight );
    } else {
		// bottom
        adView.frame = CGRectMake( ( parentSize.width - contentSizeWidth ) * 0.5, parentSize.height - contentSizeHeight - safeAreaInsets.bottom - d, contentSizeWidth, contentSizeHeight );
    }
}

-(void) FAdsEvent:(NSString *) event params:(NSDictionary< NSString*, NSString* > *) params service:(NSString *) service {
    
    NSDictionary *eventData;
    if(service == nil) {
        service =@"";
    }
    
    if([params count] > 0) {
        NSArray * allKeys = [params allKeys];
        NSMutableArray * allVals = [NSMutableArray array];
        for( NSString * key : allKeys ) {
            [allVals addObject:params[key]];
        }
        eventData = [[NSDictionary alloc] initWithObjectsAndKeys:
                     event, @"eventName",
                     service, @"service",
                     allKeys, @"eventKeys",
                     allVals, @"eventVals",
                     nil];
    }
    else {
        eventData = [[NSDictionary alloc] initWithObjectsAndKeys:
                     event, @"eventName",
                     service, @"service",
                     nil];
    }
    
    NSString *JSONString = [self dictToJSONString:eventData];
//    NSLog (@"FAdsEvent json %@", JSONString);
    
    UnitySendMessage( "FAdsManager", "AdsEvent",  [FAdsUnityWrapper NSStringToChar:JSONString] );
}

-(void) FAdsShouldReward {
    UnitySendMessage( "FAdsManager", "ShouldReward",  [FAdsUnityWrapper NSStringToChar:@"yo"]);
}

-(void) FAdsMoPubInitialized:(NSString *) privacyPolicyUrl vendorListUrl:(NSString *) vendorListUrl {
    if(privacyPolicyUrl == nil) {
        privacyPolicyUrl = @"";
    }
    
    if(vendorListUrl == nil) {
        vendorListUrl = @"";
    }
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:@{ @"privacyPolicyUrl" : privacyPolicyUrl, @"vendorListUrl" : vendorListUrl } options:0 error:nil];
    NSString *JSONString = [[NSString alloc] initWithBytes:[jsonData bytes] length:[jsonData length] encoding:NSUTF8StringEncoding];
//    NSLog (@"FAdsMoPubInitialized json %@", JSONString);
    UnitySendMessage( "FAdsManager", "SdkInitialized", [FAdsUnityWrapper NSStringToChar:JSONString] );
}

- (void) FAdsRevenueData:(NSString *)revenueData {
    if(revenueData == nil)
        return;
    
    UnitySendMessage( "FAdsManager", "AdsRevenueImpression", [FAdsUnityWrapper NSStringToChar:revenueData] );
}

-(void) FAdsSet:(NSString *) key state:(NSString *) value {
    NSString* result = [NSString stringWithFormat:@"%@%@%@", key, Separator, value];
    
//    NSLog (@"FAds stability %@:%@", key, value);
    UnitySendMessage( "FAdsManager", "StabilityLog", [FAdsUnityWrapper NSStringToChar:result] );
}

@end



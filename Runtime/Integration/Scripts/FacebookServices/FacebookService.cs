// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Threading.Tasks;
using Facebook.Unity;
using FAdsSdk;
using UnityEngine;
using Vaveda.Integration.Scripts.Analytics;
using Vaveda.Integration.Scripts.Interfaces;
using Vaveda.Integration.Scripts.Regulation;
using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Scripts.FacebookServices
{
    public class FacebookService : IAnalyticsExtended
    {
        private bool isEnabled;
        private TaskCompletionSource<bool> initializationCompletionSource;

        public EventDestination EventDestination => EventDestination.None;

        public void ConsentEvent()
        {
            //Do nothing
        }

        public async Task Initialize()
        {
            initializationCompletionSource = new TaskCompletionSource<bool>();

            if (FB.IsInitialized)
            {
                initializationCompletionSource.SetResult(true);
            }
            else
            {
                FB.Init(() => initializationCompletionSource.SetResult(true), OnHideUnity);
            }

            await initializationCompletionSource.Task;

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Facebook is initialized");
        }

        public void TutorialEndEvent(string levelName, int levelIndex, int levelTime,
            int levelResult, int levelScore = 0, int totalScore = 0)
        {
            //Do nothing
        }

        private void OnHideUnity(bool isHide)
        {
        }

        public async void SetEnable()
        {
            if (isEnabled)
            {
                return;
            }

            isEnabled = true;

            await initializationCompletionSource.Task;

            FB.Mobile.SetAutoLogAppEventsEnabled(true);
            FB.Mobile.SetAdvertiserIDCollectionEnabled(true);
            FB.ActivateApp();

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Facebook is enabled");
        }

        public Dictionary<string, string> GetAdsPostRequestParams()
        {
            //Do nothing
            return new Dictionary<string, string>();
        }

        public void ShowConsentEvent(RegulationSource regulationSource, RegulationType regulationType,
            long regulationVersion,
            string locale)
        {
            //Do nothing
        }

        public void SetRegulationData(RegulationSource source, RegulationType type, long version, string locale)
        {
            //Do nothing
        }

        public void SaveSession()
        {
            //Do nothing
        }

        public void LevelStartEvent(string levelName, int levelPosition, int levelScore = 0,
            int totalScore = 0, string levelData = null)
        {
            //Do nothing
        }

        public void LevelEndEvent(string levelName, int levelPosition, int levelTime, int levelResult,
            int levelScore, int totalScore, string levelData)
        {
            //Do nothing
        }

        public void LevelTrackEvent(string levelName, int levelPosition, int levelTime,
            int levelResult, int levelScore, int totalScore,
            int levelFlowVal, int levelFlowMax, string levelData)
        {
            //Do nothing
        }

        public void ChooseEvent(string levelName, int levelPosition, string chooseType, string chooseOption,
            int chooseResult)
        {
            //Do nothing
        }

        public void GetItemEvent(string placement, string levelName, int levelPosition, string itemType,
            string itemName,
            int itemQuantity = 1)
        {
            //Do nothing
        }

        public void OnAdsEvent(FAdsEvent fAdsEvent)
        {
            //Do nothing
        }

        public void SendEvent(string eventName, Dictionary<string, string> data)
        {
            //Do nothing
        }
    }
}
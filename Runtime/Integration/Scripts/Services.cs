﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Threading;
using System.Threading.Tasks;
using FAdsSdk;
using JetBrains.Annotations;
using UnityEngine;
using Vaveda.Integration.Scripts.Analytics;
using Vaveda.Integration.Scripts.Analytics.Builders;
using Vaveda.Integration.Scripts.Fads;
using Vaveda.Integration.Scripts.Interfaces;
using Vaveda.Integration.Scripts.Localization;
using Vaveda.Integration.Scripts.Regulation;
using Vaveda.Integration.Scripts.Utils;
using RegulationSource = Vaveda.Integration.Scripts.Regulation.RegulationSource;
using RegulationType = Vaveda.Integration.Scripts.Regulation.RegulationType;

namespace Vaveda.Integration.Scripts
{
    [RequireComponent(typeof(FadsFocus))]
    public class Services : SingletonBehaviour<Services>
    {
        [UsedImplicitly] public event Action OnInitializationComplete;
        [UsedImplicitly] public event Action OnBackPressed;

        public FadsService FadsService { get; private set; }
        public RegulationService RegulationService { get; private set; }
        public IAnalyticsExtended AnalyticsService { get; private set; }

        public bool IsInitialized { get; private set; }

        [UsedImplicitly]
        private void OnDestroy()
        {
            FadsService?.Dispose();
        }

        [UsedImplicitly]
        public async void Initialize(AnalyticsBuilder analyticsBuilder, AdShowType adMask)
        {
            await InitializeAsync(analyticsBuilder, adMask);
        }

        public async Task InitializeAsync(AnalyticsBuilder analyticsBuilder, AdShowType adMask)
        {
            Application.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Start initialization");

            AppTrackingTransparency.RegisterAppForAdNetworkAttribution();
            
            var analyticsEventsFilter = Resources.Load<TextAsset>("analytic_events_filter").text;

            RegulationService = new RegulationService();
            AnalyticsService = new AnalyticsServicesContainer(analyticsEventsFilter, analyticsBuilder);
            FadsService = new FadsService(AnalyticsService, adMask);

            StartAnalyticsInitialization();

            await FadsService.InitForConsent(RegulationService);
            await ShowTermOfPolicy();

            (RegulationSource source, RegulationType type, long version, string locale) =
                RegulationService.GetAnalyticsData();

            AnalyticsService.SetRegulationData(source, type, version, locale);
            AnalyticsService.SetEnable();

            CompleteInit(RegulationService);

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Complete initialization");
        }

        private async void StartAnalyticsInitialization()
        {
            await AnalyticsService.Initialize();
        }

        private async Task ShowTermOfPolicy()
        {
            if (Defines.IsLogEnabled)
                Debug.Log($"[FAds Vaveda] Try show term of policy. mopub: {FadsService.ShouldShowConsentDialog}");

            var termOfPolicyPrefab = Resources.Load<TermOfPolicyCanvas>("TermOfPolicyCanvas");
            var termOfPolicy = Instantiate(termOfPolicyPrefab, transform);
            var destroyTermOfPolicy = (Action)(() =>
            {
                if (null != termOfPolicy)
                {
                    Destroy(termOfPolicy.gameObject);
                }
            });

            termOfPolicy.gameObject.SetActive(false);

            if (FadsService.ShouldShowConsentDialog)
            {
                var version = Analytics.Utils.CurrentTimeSeconds();
                AnalyticsService.ShowConsentEvent(RegulationSource.FAds, RegulationType.Gdpr, version,
                    LocalizationService.Instance.CurrentLocale.Name.ToLower());
                await termOfPolicy.ShowGdpr();
                RegulationService.AcceptPrivacy(RegulationSource.FAds, RegulationType.Gdpr,
                    version);
                GrantConsent();
                destroyTermOfPolicy();
                return;
            }

            if (!RegulationService.IsRequestCompleted &&
                Application.internetReachability != NetworkReachability.NotReachable)
            {
                await Task.Run(() =>
                    SpinWait.SpinUntil(() => RegulationService.IsRequestCompleted, 5000));
            }

            if (RegulationService.IsRequestCompleted && RegulationService.ApiRegulationType != RegulationType.Error)
            {
                switch (RegulationService.ApiRegulationType)
                {
                    case RegulationType.Gdpr
                        when RegulationService.ApiRegulationVersion > RegulationService.CurrentGdprVersion:
                        AnalyticsService.ShowConsentEvent(RegulationSource.Api, RegulationType.Gdpr,
                            RegulationService.ApiRegulationVersion,
                            LocalizationService.Instance.CurrentLocale.Name.ToLower());
                        await termOfPolicy.ShowGdpr();
                        RegulationService.AcceptPrivacy(RegulationSource.Api, RegulationType.Gdpr,
                            RegulationService.ApiRegulationVersion);
                        GrantConsent();
                        break;
                    case RegulationType.Ccpa
                        when RegulationService.ApiRegulationVersion > RegulationService.CurrentCcpaVersion:
                        AnalyticsService.ShowConsentEvent(RegulationSource.Api, RegulationType.Ccpa,
                            RegulationService.ApiRegulationVersion,
                            LocalizationService.Instance.CurrentLocale.Name.ToLower());
                        await termOfPolicy.ShowCcpa();
                        RegulationService.AcceptPrivacy(RegulationSource.Api, RegulationType.Ccpa,
                            RegulationService.ApiRegulationVersion);
                        GrantConsent();
                        break;
                    case RegulationType.None:
                        if (RegulationService.CurrentGdprVersion == 0 && RegulationService.CurrentCcpaVersion == 0)
                        {
                            RegulationService.CurrentRegulationSource = RegulationSource.Api;
                            RegulationService.CurrentLastRegulationType = RegulationType.None;
                        }

                        break;
                }
            }
            else
            {
                if (RegulationService.CurrentGdprVersion == 0 && RegulationService.CurrentCcpaVersion == 0)
                {
                    var version = Analytics.Utils.CurrentTimeSeconds();
                    AnalyticsService.ShowConsentEvent(RegulationSource.App, RegulationType.Gdpr, version,
                        LocalizationService.Instance.CurrentLocale.Name.ToLower());
                    await termOfPolicy.ShowGdpr();
                    RegulationService.AcceptPrivacy(RegulationSource.App, RegulationType.Gdpr,
                        version);

                    GrantConsent();
                }
            }

            destroyTermOfPolicy();
            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] End show term of policy");
        }

        private void GrantConsent()
        {
            (RegulationSource source, RegulationType type, long version, string locale) =
                RegulationService.GetAnalyticsData();

            AnalyticsService.SetRegulationData(source, type, version, locale);
            AnalyticsService.SetEnable();

            AnalyticsService.ConsentEvent();

            FadsService.GrantConsent();
        }

        private void CompleteInit(RegulationService regulationService)
        {
            FadsService.EnableAds(regulationService);

            IsInitialized = true;
            OnInitializationComplete?.Invoke();
        }

        [UsedImplicitly]
        protected internal void BackButtonPressed()
        {
            OnBackPressed?.Invoke();
        }
    }
}
using System;
using System.Threading.Tasks;

namespace Vaveda.Integration.Scripts.Interfaces
{
    public interface IAsyncInitializable
    {
        event Action InitializeComplete;
        bool IsInitialized { get; }
        Task<bool> InitializeAsync();
    }
}
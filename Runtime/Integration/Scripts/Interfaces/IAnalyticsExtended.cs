// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using Vaveda.Integration.Scripts.Regulation;

namespace Vaveda.Integration.Scripts.Interfaces
{
    //TODO: Refactor extended analytic
    public interface IAnalyticsExtended : IAnalyticsCommon
    {
        void SetEnable();
        Dictionary<string, string> GetAdsPostRequestParams();

        void ShowConsentEvent(RegulationSource regulationSource, RegulationType regulationType, long regulationVersion,
            string locale);

        void SetRegulationData(RegulationSource source, RegulationType type, long version, string locale);

        void SaveSession();
    }
}
// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Threading.Tasks;
using FAdsSdk;
using Vaveda.Integration.Scripts.Analytics;

namespace Vaveda.Integration.Scripts.Interfaces
{
    public interface IAnalyticsCommon
    {
        EventDestination EventDestination { get; }
        void ConsentEvent();
        Task Initialize();

        void TutorialEndEvent(string levelName, int levelIndex, int levelTime, int levelResult, int levelScore = 0,
            int totalScore = 0);

        /// <summary>
        /// On level end event
        /// </summary>
        /// <param name="levelName">Level name example: "Level_8"</param>
        /// <param name="levelPosition">Level number example: "44"</param>
        /// <param name="levelScore">level score</param>
        /// <param name="totalScore">total score</param>
        /// <param name="levelData">optional json string with level data</param>
        void LevelStartEvent(string levelName, int levelPosition, int levelScore = 0, 
            int totalScore = 0, string levelData = null);
        
        /// <summary>
        /// On level end event
        /// </summary>
        /// <param name="levelName">Level name example: "Level_8"</param>
        /// <param name="levelPosition">Level number example: "44"</param>
        /// <param name="levelTime">Spend time for level in seconds</param>
        /// <param name="levelResult">0 - lose, 1 - win</param>
        /// <param name="levelScore">level score</param>
        /// <param name="totalScore">total score</param>
        /// <param name="levelData">optional json string with level data</param>
        void LevelEndEvent(string levelName, int levelPosition, int levelTime, int levelResult,
            int levelScore = 0, int totalScore = 0, string levelData = null);

        void LevelTrackEvent(string levelName, int levelPosition, int levelTime,
            int levelResult, int levelScore, int totalScore,
            int levelFlowVal, int levelFlowMax, string levelData = null);

        void ChooseEvent(string levelName, int levelPosition, string chooseType, string chooseOption,
            int chooseResult);

        void GetItemEvent(string placement, string levelName, int levelPosition, string itemType, string itemName,
            int itemQuantity = 1);

        void OnAdsEvent(FAdsEvent fAdsEvent);

        void SendEvent(string eventName, Dictionary<string, string> data);
    }
}
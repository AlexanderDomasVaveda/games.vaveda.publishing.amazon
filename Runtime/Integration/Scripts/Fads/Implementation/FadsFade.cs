// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections;
using JetBrains.Annotations;
using UnityEngine;

namespace Vaveda.Integration.Scripts.Fads
{
    public class FadsFade : MonoBehaviour
    {
        [SerializeField]
        private GameObject fade;
        
        [SerializeField]
        private Transform loadingRing;
        
        [SerializeField]
        private float rotationDuration;

        private const float FullCircleAngle = 360f;
        private bool isShow;
        
        [UsedImplicitly]
        private void Awake()
        {
            fade.SetActive(false);
        }
        
        [UsedImplicitly]
        private void OnDestroy()
        {
            StopAllCoroutines();
        }

        public void Show()
        {
            fade.SetActive(true);
            isShow = true;

            StartCoroutine(RotateLoadingRing());
        }
        
        public void Hide()
        {
            fade.SetActive(false);
            isShow = false;
        }

        private IEnumerator RotateLoadingRing()
        {
            while (isShow)
            {
                yield return null;

                var angle = FullCircleAngle * Time.deltaTime / rotationDuration;
                
                loadingRing.Rotate(0f,0f,angle, Space.Self);
            }
        }
    }
}
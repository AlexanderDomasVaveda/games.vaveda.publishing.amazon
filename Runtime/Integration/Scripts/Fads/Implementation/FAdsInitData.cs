using System;

namespace FAdsSdk
{
    [Serializable]
    internal class FAdsInitData
    {
        public string privacyPolicyUrl;
        public string vendorListUrl;
    }
}
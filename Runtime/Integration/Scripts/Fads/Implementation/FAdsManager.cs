﻿using System;
using UnityEngine;

namespace FAdsSdk
{
    public class FAdsManager : MonoBehaviour
    {
        public static FAdsManager Instance { get; private set; }

        internal static event Action<FAdsEventData> AdsEventReceived;
        internal static event Action RewardedCompleted;
        internal static event Action<FAdsInitData> Initialized;
        internal static event Action<string> AdsImpressionRevenue;

        private static readonly string[] SEPARATOR = {"!@#"};

        private void Awake()
        {
            if (Instance == null)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
                Destroy(this);
        }


        private void OnDestroy()
        {
            if (Instance == this)
                Instance = null;
        }


        /// <summary>
        /// Invoked by native plugin code
        /// </summary>
        /// <param name="argsJson">json encoded event data</param>
        public void AdsEvent(string argsJson)
        {
            var eventData = ParseAdsEventData(argsJson);
            if (AdsEventReceived != null)
                AdsEventReceived(eventData);
        }


        /// <summary>
        /// Should be passed to adjust revenue tracking
        /// Invoked by native plugin code.
        /// </summary>
        /// <param name="json">json encoded revenue data</param>
        public void AdsRevenueImpression(string json)
        {
            if (!string.IsNullOrEmpty(json))
            {
                if (AdsImpressionRevenue != null)
                {
                    AdsImpressionRevenue.Invoke(json);
                }
            }
        }

        /// <summary>
        /// return module initialization inner stacktrace
        /// Invoked by native plugin code.
        /// </summary>
        public void StabilityLog(string msg)
        {
//        string[] kv = msg.Split(SEPARATOR, StringSplitOptions.None);
//        if (kv.Length == 2)
//        {
//        }
        }

        public void ShouldReward(string dummy)
        {
            if (RewardedCompleted != null)
                RewardedCompleted();
        }

        public void SdkInitialized(string argsJson)
        {
            var initData = ParseInitEventData(argsJson);
            if (Initialized != null)
            {
                Initialized(initData);
            }
        }

        private FAdsEventData ParseAdsEventData(string json)
        {
            FAdsEventData data = JsonUtility.FromJson<FAdsEventData>(json);
            if (data == null || string.IsNullOrEmpty(data.eventName))
                return null;
            return data;
        }

        private FAdsInitData ParseInitEventData(string json)
        {
            FAdsInitData data = JsonUtility.FromJson<FAdsInitData>(json);
            return data;
        }


        /// <summary>
        /// Unity Editor method stab 
        /// </summary>
        public void FakeInit()
        {
            var initData = new FAdsInitData()
            {
                privacyPolicyUrl = "",
                vendorListUrl = ""
            };

            if (Initialized != null)
            {
                Initialized(initData);
            }
        }
    }
}
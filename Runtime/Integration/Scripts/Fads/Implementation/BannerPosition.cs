using System;

namespace FAdsSdk
{
    /// <summary>
    /// Banner screen position
    /// </summary>
    [Serializable]
    public enum BannerPosition
    {
        None,
        Bottom,
        Top
    }
}
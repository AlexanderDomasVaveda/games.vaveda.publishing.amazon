namespace FAdsSdk
{
    public class FAdsEditor : FAdsBase
    {
        static FAdsEditor()
        {
            InitManager();
        }


        public static void SetBannerPosition(BannerPosition bannerPosition, float offset)
        {
        }

        public static void InitializeSdk(bool limitTracking, bool ccpaApplies)
        {
        }

        public static void InitializeConfig(string cfg, string key)
        {
            FAdsManager.Instance.FakeInit();
        }

        public static void ShowBanner(string placemen, string tag)
        {
        }

        public static void HideBanner()
        {
        }

        public static int InterstitialReadyState()
        {
            return (int) AdInterstitialReadyState.Ready;
        }

        public static void ShowInterstitial(string placement, string tag)
        {
        }

        public static void LoadRewarded()
        {
        }

        public static int RewardedReadyState()
        {
            return (int) AdRewardedReadyState.Ready;
        }

        public static void ShowRewarded(string placement, string tag)
        {
        }

        public static int CurrentConsentStatus()
        {
            return 0;
        }

        public static void GrantConsent()
        {
        }

        public static bool ShouldShowConsentDialog()
        {
            return false;
        }

        public static int IsGDPRApplicable()
        {
            return -1;
        }

        public static void DisableAds(AdShowType cacheType)
        {
            if ((cacheType & AdShowType.Banner) == AdShowType.Banner)
            {
            }

            if ((cacheType & AdShowType.Interstitial) == AdShowType.Interstitial)
            {
            }

            if ((cacheType & AdShowType.Rewarded) == AdShowType.Rewarded)
            {
            }
        }

        public static void EnableAds(AdShowType cacheType)
        {
            if ((cacheType & AdShowType.Banner) == AdShowType.Banner)
            {
            }

            if ((cacheType & AdShowType.Interstitial) == AdShowType.Interstitial)
            {
            }

            if ((cacheType & AdShowType.Rewarded) == AdShowType.Rewarded)
            {
            }
        }

        public static void SetTestMode()
        {
        }

        public static string GetConnectionType()
        {
            return "unity_editor";
        }


        public static string GetStateLog()
        {
            return null;
        }

        public static bool IsTablet()
        {
            return false;
        }

        public static void AllowPauseRenderer(bool allow)
        {
            
        }
        
        public static string GetNativeVersion()
        {
            return "0.0.0";
        }
    }
}
using System;
using UnityEngine;

namespace FAdsSdk
{
    public class FAdsAndroid : FAdsBase
    {
        private const string helperClassName = "com.fabros.fadsproxy.FAdsUnityPlugin";
        private const string fAdsClassName = "com.fabros.fads.FAdsObject";
        private static AndroidJavaObject fAdsHelper;
        private static AndroidJavaClass fAds;

        private static bool inited;

        static FAdsAndroid()
        {
            InitManager();
        }

        public static void SetBannerPosition(BannerPosition bannerPosition, float bannerOffset)
        {
            if (!inited)
                return;

            int pos = -1;
            if (bannerPosition == BannerPosition.Bottom)
                pos = 0;
            else if (bannerPosition == BannerPosition.Top)
                pos = 1;
            if (pos == 0 || pos == 1)
                fAdsHelper.Call("setBannerPosition", pos, bannerOffset);
        }

        public static void InitializeSdk(bool limitTracking, bool ccpaApplies)
        {
            if (inited)
                return;

            try
            {
                using (AndroidJavaClass pluginClass = new AndroidJavaClass(helperClassName))
                {
                    fAdsHelper = pluginClass.CallStatic<AndroidJavaObject>("getInstance");
                    if (fAdsHelper == null)
                    {
                        Debug.LogError("plug is null");
                    }
                    else
                    {
                        using (AndroidJavaClass unityPlayerClass =
                            new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                        {
                            using (AndroidJavaObject currentActivity =
                                unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"))
                            {
                                fAdsHelper.Call("FAdsUnityInitialize", currentActivity);
                            }
                        }
                    }
                }

                fAds = new AndroidJavaClass(fAdsClassName);
                SetCCPARestricted(ccpaApplies);
                SetCCPAOptOut(limitTracking);
                inited = true;
            }
            catch (Exception e)
            {
                inited = false;
                Debug.LogError("FAdsAndroid InitializeSdk ex. " + e.Message);
                //todo log?
            }
        }

        public static void InitializeConfig(string cfg, string key)
        {
            if (!inited)
                return;

//        if (BuildConfig.LOG_ENABLED) 
//          SetTestMode();

            fAdsHelper.Call("FAdsInitializeConfig", cfg, key);
        }

        public static void ShowBanner(string placement, string tag)
        {
            if (!inited)
                return;
            fAdsHelper.Call("BannerShow", placement, tag);
        }

        public static void HideBanner()
        {
            if (!inited)
                return;
            fAdsHelper.Call("BannerHide");
        }

        public static int InterstitialReadyState()
        {
            if (!inited)
                return (int) AdInterstitialReadyState.StateNotEnabled;

            return fAds.CallStatic<int>("interstitialReadyStatus");
        }

        public static void ShowInterstitial(string placement, string tag)
        {
            if (!inited)
                return;
            fAdsHelper.Call("ShowInterstitial", placement, tag);
        }

        public static void LoadRewarded()
        {
            if (!inited)
                return;

            fAdsHelper.Call("EnableRewarded", true);
        }

        public static int RewardedReadyState()
        {
            if (!inited)
                return (int) AdRewardedReadyState.StateNotEnabled;

            return fAds.CallStatic<int>("rewardedReadyStatus");
        }

        public static void ShowRewarded(string placement, string tag)
        {
            if (!inited)
                return;
            fAdsHelper.Call("ShowRewarded", placement, tag);
        }

        public static void GrantConsent()
        {
            if (!inited)
                return;
            fAdsHelper.Call("GrantConsent");
        }

        public static bool ShouldShowConsentDialog()
        {
            if (!inited)
                return true;

            return fAds.CallStatic<bool>("isNeedShowConsent");
        }

        public static int IsGDPRApplicable()
        {
            if (!inited)
                return 1;

            return fAdsHelper.Call<int>("isGDPRApplicable");
        }
        
        public static void DisableAds(AdShowType cacheType)
        {
            if (!inited)
                return;

            if ((cacheType & AdShowType.Banner) == AdShowType.Banner)
            {
                fAdsHelper.Call("EnableBanner", false);
            }

            if ((cacheType & AdShowType.Interstitial) == AdShowType.Interstitial)
            {
                fAdsHelper.Call("EnableInterstitial", false);
            }

            if ((cacheType & AdShowType.Rewarded) == AdShowType.Rewarded)
            {
                fAdsHelper.Call("EnableRewarded", false);
            }
        }

        public static void EnableAds(AdShowType cacheType)
        {
            if (!inited)
                return;

            if ((cacheType & AdShowType.Banner) == AdShowType.Banner)
            {
                fAdsHelper.Call("EnableBanner", true);
            }

            if ((cacheType & AdShowType.Interstitial) == AdShowType.Interstitial)
            {
                fAdsHelper.Call("EnableInterstitial", true);
            }

            if ((cacheType & AdShowType.Rewarded) == AdShowType.Rewarded)
            {
                fAdsHelper.Call("EnableRewarded", true);
            }
        }

        public static void SetTestMode()
        {
            if (!inited)
                return;
            fAds.CallStatic("setLog", true);
        }

        public static string GetConnectionType()
        {
            return fAdsHelper.Call<string>("GetConnectionType");
        }

        public static string GetStateLog()
        {
            if (!inited)
                return null;

            return fAds.CallStatic<string>("getLogStackTrace");
        }

        private static void SetCCPAOptOut(bool value)
        {
            if (!inited)
                return;
            fAds.CallStatic("setCCPAOptOut", value);
        }

        private static void SetCCPARestricted(bool value)
        {
            if (!inited)
                return;
            fAds.CallStatic("setCCPARestricted", value);
        }

        public static bool IsTablet()
        {
            if (null == fAds)
            {
                fAds = new AndroidJavaClass(fAdsClassName);
            }

            using (AndroidJavaClass unityPlayerClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
            {
                using (AndroidJavaObject currentActivity =
                    unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"))
                {
                    return fAds.CallStatic<bool>("isTabletScreen", currentActivity);
                }
            }
        }

        public static void AllowPauseRenderer(bool allow)
        {
            //unused. ios feature only
        }

        public static string GetNativeVersion()
        {
            try
            {
                using (AndroidJavaClass androidJavaClass = new AndroidJavaClass(fAdsClassName))
                {
                    return androidJavaClass.GetStatic<string>("MODULE_VERSION");
                }
            }
            catch (Exception)
            {
                return "0.0.0";
            }
        }
    }
}
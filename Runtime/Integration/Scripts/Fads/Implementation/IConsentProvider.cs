﻿using System;

namespace FAdsSdk
{
    public interface IConsentProvider
    {
        event Action ConsentInitialized;
        string PolicyURL { get; }
        string VendorsURL { get; }
        void InitForConsent(bool ccpaApplied, bool limitTracking);
        void GrantConsent();
        bool ShouldShowConsentDialog { get; }
        bool? IsGDPRApplicable { get; }
    }
}


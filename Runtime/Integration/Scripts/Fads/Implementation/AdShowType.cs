using System;

namespace FAdsSdk
{
    [Serializable]
    [Flags]
    public enum AdShowType
    {
        None = 0,
        Banner = 1, 
        Interstitial = 2, 
        Rewarded = 4,
        Everything = ~None
    }
}
namespace FAdsSdk
{
    public enum AdInterstitialReadyState
    {
        Ready = 0,
        SomethingPresent = 1,
        StateNotEnabled = 2,
        NotCached = 3,
        SkipByInterDelay = 4,
        SkipByRewardedDelay = 5
    }

    public enum AdRewardedReadyState
    {
        Ready = 0,
        StateNotEnabled = 2,
        NotCached = 3
    }
}
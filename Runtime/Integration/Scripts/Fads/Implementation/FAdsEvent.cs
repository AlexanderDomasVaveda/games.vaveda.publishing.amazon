using System.Collections.Generic;

namespace FAdsSdk
{
    /// <summary>
    /// Event data
    /// </summary>
    public class FAdsEvent
    {
        /// <summary>
        /// event name
        /// </summary>
        public string name;

        /// <summary>
        /// Can be null. event key-value pairs
        /// </summary>
        public Dictionary<string, string> parameters;

        public EventAnalyticDestination destination;

        public FAdsEvent(string name, Dictionary<string, string> parameters, EventAnalyticDestination destination)
        {
            this.name = name;
            this.parameters = parameters;
            this.destination = destination;
        }
    }
}
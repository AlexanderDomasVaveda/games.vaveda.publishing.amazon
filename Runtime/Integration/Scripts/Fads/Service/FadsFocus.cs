// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using JetBrains.Annotations;
using UnityEngine;
using Vaveda.Integration.Scripts.Regulation;
using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Scripts.Fads
{
    internal class FadsFocus : MonoBehaviour
    {
        private const double MinTime = 3.0;
        private DateTime timer;

        [UsedImplicitly]
        private async void OnApplicationFocus(bool focus)
        {
            var services = Services.Instance;

            if (!services.IsInitialized)
            {
                return;
            }

            var fads = Services.Instance.FadsService;

            if (Defines.IsLogEnabled)
                Debug.Log($"[FAds Vaveda] OnApplicationFocus: {focus}; Is ad showing: {fads.IsAdShowing}");

            if (focus)
            {
                services.AnalyticsService.SaveSession();

                var deltaTime = DateTime.UtcNow - timer;

                if (deltaTime.TotalSeconds >= MinTime && !fads.IsAdShowing && Defines.IsAndroid)
                {
                    await LoadingOverlay.Instance.Show();
                    await fads.ShowInterstitial(Placements.PLACEMENT_INTERSTITIAL_APP_RESUME);
                    LoadingOverlay.Instance.Hide();
                }
                else
                {
                    fads.ShowBannerIfHidden();
                }
            }
            else
            {
                services.AnalyticsService.SaveSession();
                
                fads.HideBannerIfShowed();

                timer = DateTime.UtcNow;
            }
        }

        [UsedImplicitly]
        private void Update()
        {
            if (Input.GetKeyUp(KeyCode.Escape))
            {
                Services.Instance.BackButtonPressed();
            }
        }
    }
}
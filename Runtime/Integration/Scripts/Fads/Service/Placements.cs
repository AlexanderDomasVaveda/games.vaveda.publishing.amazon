namespace Vaveda.Integration.Scripts.Fads
{
    public class Placements
    {
        public const string PLACEMENT_INTERSTITIAL_LEVEL_START = "level_start";
        public const string PLACEMENT_INTERSTITIAL_LEVEL_END = "level_end";
        public const string PLACEMENT_INTERSTITIAL_LEVEL_RESUME = "level_resume";
        public const string PLACEMENT_INTERSTITIAL_LEVEL_RESTART = "level_restart";
        public const string PLACEMENT_INTERSTITIAL_APP_RESUME = "app_resume";
        public const string PLACEMENT_REWARDED_LEVEL_SKIP = "level_skip";
        public const string PLACEMENT_REWARDED_LEVEL_XPOINTS = "level_xpoints";
        public const string PLACEMENT_REWARDED_LEVEL_BONUS = "level_bonus";
        public const string PLACEMENT_REWARDED_LEVEL_CONTINUE = "level_continue";
        public const string PLACEMENT_REWARDED_LEVEL_GETKEYS = "level_getkeys";
        public const string PLACEMENT_REWARDED_LEVEL_GETHINTS = "level_gethints";
        public const string PLACEMENT_REWARDED_LEVEL_GETSHUFFLES = "level_getshuffles";
        public const string PLACEMENT_REWARDED_LEVEL_RESTART = "level_restart";
        public const string PLACEMENT_BANNER_LEVEL = "level";
        public const string PLACEMENT_BANNER_EVERYWHERE = "everywhere";
        public const string PLACEMENT_GETITEM_LEVEL_START = "level_start";
        public const string PLACEMENT_GETITEM_LEVEL_END = "level_end";
        public const string PLACEMENT_GETITEM_LEVEL_BONUS = "level_bonus";
        public const string PLACEMENT_GETITEM_CHAPTER_BONUS = "chapter_bonus";
        public const string PLACEMENT_GETITEM_CHAPTER_END = "chapter_end";
        public const string PLACEMENT_GETITEM_GAME_START = "game_start";
    }
}
// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Threading;
using System.Threading.Tasks;
using FAdsSdk;
using Vaveda.Integration.Scripts.Interfaces;
using UnityEngine;
using Vaveda.Integration.Scripts.Regulation;
using Vaveda.Integration.Scripts.Utils;
using Debug = UnityEngine.Debug;

namespace Vaveda.Integration.Scripts.Fads
{
    public class FadsService : IDisposable
    {
        private const int DelayAfterInterstitial = 50;
        private const int TestDelayAfterInterstitial = 5000;

        private const int TickToUpdateVideo = 1000;
        private int RewardedVideoCompleteWaitTime = 1200;

        private const string AdsConfigUrl = "https://ads01.vaveda.games/api/config";

        private readonly IAnalyticsExtended analytics;
        private readonly FAdsNetwork fAdsNetwork;

        private readonly CancellationTokenSource rewardedVideoUpdateCancellationToken;
        private readonly AdShowType adsMask;

        private int minLevelIndexThreshold;
        private int currentLevelIndex;

        private bool isBannerShowed;
        private string currentBannerPlacement;

        public event Action RewardedVideoComplete;
        public event Action RewardedVideoIncomplete;
        public event Action<bool> RewardedVideoLoaded;

        public bool ShouldShowConsentDialog => fAdsNetwork?.ShouldShowConsentDialog ?? false;
        public bool IsAdsEnable { get; private set; }
        public bool IsAdShowing { get; private set; }

        /// <summary>
        /// Check can show interstitial by levels
        /// </summary>
        /// <returns></returns>
        private bool CanShowInterstitial =>
            currentLevelIndex >= minLevelIndexThreshold;

        public bool HasInterstitial => CanShowInterstitial && (fAdsNetwork?.HasInterstitial(false, "") ?? false);
        public bool HasRewardedVideo => fAdsNetwork?.HasRewardedVideo(false, "") ?? false;

        public FadsService(IAnalyticsExtended analytics, AdShowType adsMask = AdShowType.None)
        {
            this.adsMask = adsMask;

            if (adsMask == AdShowType.None)
            {
                return;
            }

            rewardedVideoUpdateCancellationToken = new CancellationTokenSource();
            this.analytics = analytics;

            fAdsNetwork = new FAdsNetwork();
            fAdsNetwork.DisableAds(AdShowType.Everything);
            fAdsNetwork.AdsEvent += OnAdsEvent;
        }

        public async Task InitForConsent(RegulationService regulationService)
        {
            if (null == fAdsNetwork)
            {
                if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Error: FAdsNetwork is null");
                return;
            }

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Start init consent");

            var tcs = new TaskCompletionSource<bool>();
            var setResult = (Action)(() => tcs.SetResult(true));

            fAdsNetwork.ConsentInitialized += setResult;
            fAdsNetwork.InitForConsent(regulationService.IsCcpaApplies, regulationService.PersistentOptout);

            await tcs.Task;

            fAdsNetwork.ConsentInitialized -= setResult;

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Complete init consent");
        }

        public async void EnableAds(RegulationService regulationService)
        {
            if (null == fAdsNetwork)
            {
                return;
            }

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Start enable ads");

            fAdsNetwork.EnableAds(adsMask);
            IsAdsEnable = true;

            if (!Defines.IsAmazon)
            {
                var cfg = await GetAdsConfig();

                if (Defines.IsLogEnabled)
                    Debug.Log($"[FAds Vaveda] Configuration from server response: [{cfg ?? "null"}]");

                if (cfg != null)
                {
                    fAdsNetwork.Initialize(cfg, regulationService.PersistentOptout, regulationService.IsCcpaApplies);
                }
            }

            if (adsMask.HasFlag(AdShowType.Rewarded))
            {
                UpdateRewardedVideoLoaded(rewardedVideoUpdateCancellationToken.Token);
            }

            fAdsNetwork.AllowPauseRenderer(true);
            fAdsNetwork.SetBannerPosition(BannerPosition.Bottom, 0);
            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Ads enable complete");
        }

        public void GrantConsent()
        {
            if (null == fAdsNetwork)
            {
                return;
            }

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] MoPub GrantConsent");
            fAdsNetwork.GrantConsent();
        }

        public void Dispose()
        {
            rewardedVideoUpdateCancellationToken?.Cancel();
        }

        private void OnAdsEvent(FAdsEvent e)
        {
            if (null == fAdsNetwork)
            {
                return;
            }

            analytics.OnAdsEvent(e);
        }

        private async Task<string> GetAdsConfig()
        {
            if (null == fAdsNetwork)
            {
                return string.Empty;
            }

            try
            {
                if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Try get config");

                var url = new Uri(AdsConfigUrl);
                var requestData = analytics?.GetAdsPostRequestParams();
                var config = await new SimpleWebRequest(url).Post(requestData);

                return config;
            }
            catch (Exception ex)
            {
                if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] Get config exception: [{ex.Message}]");
                return string.Empty;
            }
        }

        private static float GetBottomOffset()
        {
            return Screen.safeArea.position.y;
        }

        private void SetBannerPosition(BannerPosition position)
        {
            fAdsNetwork.SetBannerPosition(position, GetBottomOffset());
        }

        public void ShowBanner(string placement)
        {
            if (null == fAdsNetwork)
            {
                return;
            }

            isBannerShowed = true;
            currentBannerPlacement = placement;
            fAdsNetwork.ShowBanner(placement);
        }

        public void HideBanner()
        {
            if (null == fAdsNetwork)
            {
                return;
            }

            isBannerShowed = false;
            fAdsNetwork.HideBanner();
        }

        internal void ShowBannerIfHidden()
        {
            if (isBannerShowed)
            {
                fAdsNetwork?.ShowBanner(currentBannerPlacement);
            }
        }

        internal void HideBannerIfShowed()
        {
            fAdsNetwork?.HideBanner();
        }

        /// <summary>
        /// Вызов рекламы Interstitial с задержкой после старта показа. Использует асинхронный подход
        /// </summary>
        /// <param name="placement">Названия плейсмента, берется из статического класса Placements</param>
        /// <returns></returns>
        public async Task ShowInterstitial(string placement)
        {
            if (null == fAdsNetwork)
            {
                return;
            }

            if (!fAdsNetwork.HasInterstitial(true, placement))
            {
                if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Has no interstitial");
                return;
            }

            if (!CanShowInterstitial)
            {
                if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] Can't show interstitial, " +
                                                    $"{currentLevelIndex} < {minLevelIndexThreshold}");
                return;
            }

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Start show interstitial");

            fAdsNetwork.ShowInterstitial(placement);
            IsAdShowing = true;

            var delay = Defines.IsLogEnabled ? TestDelayAfterInterstitial : DelayAfterInterstitial;

            await Task.Delay(delay);

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Complete show interstitial");

            IsAdShowing = false;
        }

        public void SetFirstLevelInterstitial(int level)
        {
            minLevelIndexThreshold = level;
        }

        public void SetLevelIndex(int levelIndex)
        {
            currentLevelIndex = levelIndex;
        }

        /// <summary>
        /// Show rewarded video
        /// </summary>
        /// <param name="placement">Event where is call, take from <see cref="Placements"/></param>
        public async void ShowRewardedVideo(string placement)
        {
            await ShowRewardedVideoAsync(placement);
        }

        ///<inheritdoc cref="ShowRewardedVideo"/>
        /// <returns>Return true if complete</returns>
        public async Task<bool> ShowRewardedVideoAsync(string placement)
        {
            if (null == fAdsNetwork)
            {
                return false;
            }

            if (!fAdsNetwork.HasRewardedVideo(true, placement))
            {
                if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Has no rewarded");
                return false;
            }

            HideBannerIfShowed();

            await LoadingOverlay.Instance.Show();

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Start rewarded");

            IsAdShowing = true;

            var isRewardedComplete = false;
            var rewardCompleteAction = (EventHandler)((o, e) =>
            {
                isRewardedComplete = true;
                LoadingOverlay.Instance.Hide();
            });

            fAdsNetwork.RewardedComplete += rewardCompleteAction;
            fAdsNetwork.ShowRewardedVideo(placement);

            var maxFrames = Application.targetFrameRate <= 0 ? 60 : Application.targetFrameRate;
            var framesCount = maxFrames * RewardedVideoCompleteWaitTime / 1000;

            for (int i = 0; i < framesCount; i++)
            {
                if (isRewardedComplete)
                {
                    break;
                }

                if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] Yield {i}");
                await Task.Yield();
            }

            LoadingOverlay.Instance.Hide();
            ShowBannerIfHidden();

            if (isRewardedComplete)
            {
                if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Rewarded complete");
                RewardedVideoComplete?.Invoke();
            }
            else
            {
                if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Rewarded incomplete");
                RewardedVideoIncomplete?.Invoke();
            }

            IsAdShowing = false;

            fAdsNetwork.RewardedComplete -= rewardCompleteAction;

            return isRewardedComplete;
        }

        /// <summary>
        /// Call event <see cref="RewardedVideoLoaded"/>
        /// Every tick check rewarded video
        /// </summary>
        /// <param name="token">Cancel on dispose</param>
        private async void UpdateRewardedVideoLoaded(CancellationToken token)
        {
            void LogCancel()
            {
                if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Update rewarded video loaded task canceled");
            }

            try
            {
                while (true)
                {
                    await Task.Delay(TickToUpdateVideo, token);

                    if (fAdsNetwork.HasRewardedVideo(false, ""))
                    {
                        RewardedVideoLoaded?.Invoke(HasRewardedVideo);
                    }

                    token.ThrowIfCancellationRequested();
                }
            }
            catch (TaskCanceledException)
            {
                LogCancel();
            }
            catch (OperationCanceledException)
            {
                LogCancel();
            }

            // ReSharper disable once FunctionNeverReturns
        }
    }
}
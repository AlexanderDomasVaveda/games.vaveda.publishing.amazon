// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Playables;

namespace Vaveda.Integration.Scripts.Fads
{
    internal class LoadingOverlay : MonoBehaviour
    {
        private const float AnimationDelay = 0.1f;
        
        private static LoadingOverlay instance;
        internal static LoadingOverlay Instance
        {
            get
            {
                if (instance != null)
                {
                    return instance;
                }

                instance = FindObjectOfType<LoadingOverlay>();

                if (instance != null)
                {
                    return instance;
                }

                var overlayPrefab = Resources.Load<LoadingOverlay>("LoadingOverlay");
                    
                instance = Instantiate(overlayPrefab);

                return instance;
            }
        }

        [SerializeField]
        private PlayableDirector playableDirector;

        [SerializeField]
        private PlayableAsset showAnimation;

        [UsedImplicitly]
        private void OnEnable()
        {
            DontDestroyOnLoad(gameObject);
        }

        internal async Task Show()
        {
            gameObject.SetActive(true);
            playableDirector.Play(showAnimation);
            
            await Task.Delay(TimeSpan.FromSeconds(AnimationDelay));
        }

        internal void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
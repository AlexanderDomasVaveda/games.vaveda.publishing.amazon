// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FAdsSdk;
using UnityEngine;
using Vaveda.Integration.Scripts.Interfaces;
using Vaveda.Integration.Scripts.Regulation;
using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Scripts.Analytics
{
    public class VavedaAnalyticsService : IAnalyticsExtended
    {
        private readonly DefaultMessagesService defaultMessagesService;
        private const string StatsUrl = "https://api01.vaveda.games/event";

        public VavedaAnalyticsService(DefaultMessagesService defaultMessagesService)
        {
            this.defaultMessagesService = defaultMessagesService;
        }

        public EventDestination EventDestination => EventDestination.Vaveda;
        private bool isSendPrivateData;

        public Task Initialize()
        {
            InstallEvent();

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] VavedaAnalytic is initialized");

            return Task.CompletedTask;
        }


        private void InstallEvent()
        {
            if (PersistentData.Analytics.IsInstallEventSent)
            {
                return;
            }

            var installData = new Dictionary<string, string>
            {
                {"event", "install"},
                {"session_id", defaultMessagesService.DefaultParams["session_id"]},
                {"os_name", defaultMessagesService.DefaultParams["os_name"]},
                {"os_version", defaultMessagesService.DefaultParams["os_version"]},
                {"api_version", defaultMessagesService.DefaultParams["api_version"]},
                {"device_type", defaultMessagesService.DefaultParams["device_type"]},
                {"device_model", defaultMessagesService.DefaultParams["device_model"]},
                {"locale", defaultMessagesService.DefaultParams["locale"]},
                {"package_name", defaultMessagesService.DefaultParams["package_name"]},
                {"app_version", defaultMessagesService.DefaultParams["app_version"]},
                {"sdk_version", defaultMessagesService.DefaultParams["sdk_version"]},
                {"sdk_native_version", defaultMessagesService.DefaultParams["sdk_native_version"]},
                {"install_time", defaultMessagesService.DefaultParams["install_time"]}
            };

            if (defaultMessagesService.DefaultParams.ContainsKey("limit_ad_tracking"))
            {
                installData.Add("limit_ad_tracking", defaultMessagesService.DefaultParams["limit_ad_tracking"]);
            }

            PostOur(installData, result =>
                PersistentData.Analytics.IsInstallEventSent = result);
        }

        public void ConsentEvent()
        {
            var consentData = new Dictionary<string, string> {{"event", "consent"},};
            foreach (var keyValuePair in defaultMessagesService.DefaultParams)
            {
                consentData.Add(keyValuePair.Key, keyValuePair.Value);
            }

            PostOur(consentData);
        }

        public void ShowConsentEvent(RegulationSource regulationSource, RegulationType regulationType,
            long regulationVersion, string locale)
        {
            var showConsentData = new Dictionary<string, string>
            {
                {"event", "show_consent"},
                {"session_id", defaultMessagesService.DefaultParams["session_id"]},
                {"os_name", defaultMessagesService.DefaultParams["os_name"]},
                {"os_version", defaultMessagesService.DefaultParams["os_version"]},
                {"api_version", defaultMessagesService.DefaultParams["api_version"]},
                {"device_type", defaultMessagesService.DefaultParams["device_type"]},
                {"device_model", defaultMessagesService.DefaultParams["device_model"]},
                {"locale", defaultMessagesService.DefaultParams["locale"]},
                {"package_name", defaultMessagesService.DefaultParams["package_name"]},
                {"app_version", defaultMessagesService.DefaultParams["app_version"]},
                {"sdk_version", defaultMessagesService.DefaultParams["sdk_version"]},
                {"sdk_native_version", defaultMessagesService.DefaultParams["sdk_native_version"]},
                {"install_time", defaultMessagesService.DefaultParams["install_time"]},
                {"regulation_source", regulationSource.ToString().ToLower()},
                {"regulation_type", regulationType.ToString().ToLower()},
                {"regulation_version", regulationVersion.ToString()},
                {"consent_locale", locale}
            };

            if (defaultMessagesService.DefaultParams.ContainsKey("limit_ad_tracking"))
            {
                showConsentData.Add("limit_ad_tracking", defaultMessagesService.DefaultParams["limit_ad_tracking"]);
            }

            PostOur(showConsentData);
        }

        public void StartEvent()
        {
            var startData = new Dictionary<string, string> {{"event", "start"},};
            foreach (var keyValuePair in defaultMessagesService.DefaultParams)
            {
                startData.Add(keyValuePair.Key, keyValuePair.Value);
            }

            PostOur(startData);
        }

        public void TutorialEndEvent(string levelName, int levelIndex, int levelTime,
            int levelResult, int levelScore = 0, int totalScore = 0)
        {
            var tutorialEndData = new Dictionary<string, string>
            {
                {"event", "tutorial_end"},
                {"level_name", levelName},
                {"level_pos", levelIndex.ToString()},
                {"level_time", levelTime.ToString()},
                {"level_result", levelResult.ToString()},
                {"level_score", levelScore.ToString()},
                {"total_score", totalScore.ToString()}
            };
            foreach (var keyValuePair in defaultMessagesService.DefaultParams)
            {
                tutorialEndData.Add(keyValuePair.Key, keyValuePair.Value);
            }

            PostOur(tutorialEndData);
        }

        public void LevelStartEvent(string levelName, int levelPosition, int levelScore,
            int totalScore, string levelData)
        {
            var levelEndData = new Dictionary<string, string>
            {
                {"event", "level_start"},
                {"level_name", levelName},
                {"level_pos", levelPosition.ToString()},
                {"level_score", levelScore.ToString()},
                {"total_score", totalScore.ToString()}
            };
            if (!String.IsNullOrEmpty(levelData))
            {
                levelEndData.Add("level_data", levelData);
            }

            foreach (var keyValuePair in defaultMessagesService.DefaultParams)
            {
                levelEndData.Add(keyValuePair.Key, keyValuePair.Value);
            }

            PostOur(levelEndData);
        }

        public void LevelEndEvent(string levelName, int levelPosition, int levelTime, int levelResult,
            int levelScore, int totalScore, string levelData)
        {
            var levelEndData = new Dictionary<string, string>
            {
                {"event", "level_end"},
                {"level_name", levelName},
                {"level_pos", levelPosition.ToString()},
                {"level_time", levelTime.ToString()},
                {"level_result", levelResult.ToString()},
                {"level_score", levelScore.ToString()},
                {"total_score", totalScore.ToString()}
            };
            if (!String.IsNullOrEmpty(levelData))
            {
                levelEndData.Add("level_data", levelData);
            }

            foreach (var keyValuePair in defaultMessagesService.DefaultParams)
            {
                levelEndData.Add(keyValuePair.Key, keyValuePair.Value);
            }

            PostOur(levelEndData);
        }

        public void LevelTrackEvent(
            string levelName, int levelPosition, int levelTime,
            int levelResult, int levelScore, int totalScore,
            int levelFlowVal, int levelFlowMax, string levelData = null)
        {
            var levelTrackData = new Dictionary<string, string>
            {
                {"event", "level_track"},
                {"level_name", levelName},
                {"level_pos", levelPosition.ToString()},
                {"level_time", levelTime.ToString()},
                {"level_result", levelResult.ToString()},
                {"level_score", levelScore.ToString()},
                {"total_score", totalScore.ToString()},
                {"level_flow_val", levelFlowVal.ToString()},
                {"level_flow_max", levelFlowMax.ToString()}
            };
            if (!String.IsNullOrEmpty(levelData))
            {
                levelTrackData.Add("level_data", levelData);
            }

            foreach (var keyValuePair in defaultMessagesService.DefaultParams)
            {
                levelTrackData.Add(keyValuePair.Key, keyValuePair.Value);
            }

            PostOur(levelTrackData);
        }

        public void ChooseEvent(string levelName, int levelPosition, string chooseType, string chooseOption,
            int chooseResult)
        {
            var chooseData = new Dictionary<string, string>
            {
                {"event", "choose"},
                {"level_name", levelName},
                {"level_pos", levelPosition.ToString()},
                {"choose_type", chooseType},
                {"choose_option", chooseOption},
                {"choose_result", chooseResult.ToString()}
            };
            foreach (var keyValuePair in defaultMessagesService.DefaultParams)
            {
                chooseData.Add(keyValuePair.Key, keyValuePair.Value);
            }

            PostOur(chooseData);
        }

        public void GetItemEvent(string placement, string levelName, int levelPosition, string itemType,
            string itemName,
            int itemQuantity = 1)
        {
            var itemData = new Dictionary<string, string>
            {
                {"event", "get_item"},
                {"placement", placement},
                {"level_name", levelName},
                {"level_pos", levelPosition.ToString()},
                {"item_type", itemType},
                {"item_name", itemName},
                {"item_qty", itemQuantity.ToString()}
            };
            foreach (var keyValuePair in defaultMessagesService.DefaultParams)
            {
                itemData.Add(keyValuePair.Key, keyValuePair.Value);
            }

            PostOur(itemData);
        }

        public void SendEvent(string eventName, Dictionary<string, string> data)
        {
            OnAdsEvent(new FAdsEvent(eventName, data, EventAnalyticDestination.Everywhere));
        }

        public void OnAdsEvent(FAdsEvent fAdsEvent)
        {
            if (!isSendPrivateData)
            {
                return;
            }

            if (fAdsEvent.parameters == null)
                return;

            fAdsEvent.parameters.Add("event", fAdsEvent.name);

            foreach (var keyValuePair in defaultMessagesService.DefaultParams)
            {
                fAdsEvent.parameters.Add(keyValuePair.Key, keyValuePair.Value);
            }

            PostOur(fAdsEvent.parameters);
        }

        private async void PostOur(Dictionary<string, string> data, Action<bool> callback = null)
        {
            defaultMessagesService.CheckSession();
            data.TryGetValue("event", out var eventName);
           
            if (Defines.IsLogEnabled)
                Debug.Log($"[FAds Vaveda] Event: {eventName}, data: {data.Count}");
            try
            {
                var response = await new SimpleWebRequest(new Uri(StatsUrl)).Post(data);

                if (Defines.IsLogEnabled)
                    Debug.Log($"[FAds Vaveda] Event: {eventName}; Response from {StatsUrl}: '{response}'");

                callback?.Invoke(true);
            }
            catch (Exception e)
            {
                callback?.Invoke(false);

                if (Defines.IsLogEnabled)
                    Debug.Log($"[FAds Vaveda] Event exc.: {eventName}; URL: {StatsUrl}; Error: {e.Message}");
            }
        }

        public void SetEnable()
        {
            if (isSendPrivateData)
            {
                return;
            }

            isSendPrivateData = true;
            StartEvent();

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] VavedaAnalytic is enabled");
        }

        public Dictionary<string, string> GetAdsPostRequestParams()
        {
            //Do nothing
            return new Dictionary<string, string>();
        }

        public void SetRegulationData(RegulationSource source, RegulationType type, long version, string locale)
        {
            //Do nothing
        }

        public void SaveSession()
        {
            defaultMessagesService.SaveSessionInfo();
        }
    }
}
// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using Vaveda.Integration.Scripts.Interfaces;

namespace Vaveda.Integration.Scripts.Analytics.Builders
{
    public class AnalyticsBuilder
    {
        public List<Type> Types { get; } = new List<Type>();

        public AnalyticsBuilder Add<TA>() where TA : IAnalyticsCommon
        {
            var type = typeof(TA);
            
            if (!Types.Contains(type))
            {
                Types.Add(type);
            }
            
            return this;
        }

        public AnalyticsBuilder All()
        {
            var type = typeof(IAnalyticsCommon);
            
            Types.AddRange(AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .Where(t => type.IsAssignableFrom(t) && t.IsClass && !t.IsAbstract)
                .Where(t => !Types.Contains(t)));
            
            return this;
        }
    }
}
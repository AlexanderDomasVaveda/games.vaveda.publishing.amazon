// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

// ReSharper disable AccessToStaticMemberViaDerivedType

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using FAdsSdk;
using UnityEngine;
using Vaveda.Integration.Scripts.Regulation;
using Vaveda.Integration.Scripts.Utils;
using static Vaveda.Integration.Scripts.Regulation.PersistentData.Analytics;

namespace Vaveda.Integration.Scripts.Analytics
{
    public class DefaultMessagesService
    {
        private readonly Dictionary<string, string> defaultParams = new Dictionary<string, string>();

        public Dictionary<string, string> DefaultParams => defaultParams;

        private readonly int sessionTimeoutDuration = 1800;

        static readonly object locker = new object();
        private int mLastEventTime;
        private string mSessionId;

        public Task<bool> InitializeDefaultMessagesService()
        {
            LoadSessionInfo();
            CheckSession();
            defaultParams.Add("user_id", UserId);

            defaultParams.Add("os_name", Utils.GetOsName());
            defaultParams.Add("os_version", Utils.GetOsVersion());
            defaultParams.Add("api_version", "1.0.0");
            defaultParams.Add("pub_version", GetPublishingSdkVersion());

            defaultParams.Add("device_type", FAds.IsTablet() ? "tablet" : "phone");
            defaultParams.Add("device_model", SystemInfo.deviceModel);
            defaultParams.Add("locale", Application.systemLanguage.ToString().ToLower());
            defaultParams.Add("package_name", Application.identifier);
            defaultParams.Add("app_version", Application.version);
            defaultParams.Add("sdk_version", FAdsNetwork.SDK_VERSION);
            defaultParams.Add("sdk_native_version", FAds.GetNativeVersion());
            defaultParams.Add("install_time", InstallTime);

            defaultParams.Add("alt_id", Utils.GetAltId());

            if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] RequestAdvtId");

            var taskCompletionSource = new TaskCompletionSource<bool>();

            if (Defines.IsAmazon)
            {
                GetAmazonAdvertisingIdentifier();
                taskCompletionSource.SetResult(true);
            }
            else if (Defines.IsAndroid)
            {
                GetAndroidAdvertisingIdentifier();
                taskCompletionSource.SetResult(true);
            }
            else
            {
                var hasAdvertisingIdentifier =
                    Application.RequestAdvertisingIdentifierAsync((advertisingId, trackingEnabled, error) =>
                    {
                        if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] Receive advt_id: {advertisingId}");
                        defaultParams.Add("ad_id", advertisingId.ToString());

                        defaultParams.Add("limit_ad_tracking", trackingEnabled
                                              ? "0"
                                              : "1");

                        taskCompletionSource.SetResult(string.IsNullOrEmpty(error));
                    });

                if (!hasAdvertisingIdentifier)
                {
                    taskCompletionSource.SetResult(false);
                }
            }

            return taskCompletionSource.Task;
        }

        private void GetAndroidAdvertisingIdentifier()
        {
            AndroidJavaClass up = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
            AndroidJavaObject currentActivity = up.GetStatic<AndroidJavaObject>("currentActivity");

            AndroidJavaClass client =
                new AndroidJavaClass("com.google.android.gms.ads.identifier.AdvertisingIdClient");

            AndroidJavaObject adInfo =
                client.CallStatic<AndroidJavaObject>("getAdvertisingIdInfo", currentActivity);

            string advtId = adInfo?.Call<string>("getId") ?? "00000000" + Guid.NewGuid().ToString().Substring(8);
            bool isLimitAdTrackingEnabled = adInfo?.Call<bool>("isLimitAdTrackingEnabled") ?? false;
           
            defaultParams.Add("ad_id", advtId);
            defaultParams.Add("limit_ad_tracking", isLimitAdTrackingEnabled
                                  ? "1"
                                  : "0");
           
            if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] Receive advt_id: {advtId}");
        }

        private void GetAmazonAdvertisingIdentifier()
        {
            string advtId = null;
            int limitAdTracking = 0;
            try
            {
                using (AndroidJavaClass unityPlayerClass =
                    new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
                {
                    using (AndroidJavaObject currentActivity =
                        unityPlayerClass.GetStatic<AndroidJavaObject>("currentActivity"))
                    {
                        var contentResolver = currentActivity.Call<AndroidJavaObject>("getContentResolver");
                        AndroidJavaClass secureSettings = new AndroidJavaClass("android.provider.Settings$Secure");
                        advtId = secureSettings.CallStatic<string>("getString", contentResolver, "advertising_id");
                        limitAdTracking =
                            secureSettings.CallStatic<int>("getInt", contentResolver, "limit_ad_tracking", 0);
                    }
                }
            }
            catch (Exception)
            {
                // ignored
            }

            if (String.IsNullOrEmpty(advtId))
            {
                advtId = "00000000" + Guid.NewGuid().ToString().Substring(8);
            }

            defaultParams.Add("ad_id", advtId);
            defaultParams.Add("limit_ad_tracking", limitAdTracking.ToString());

            if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] Receive advt_id: {advtId}");
        }

        private static string GetPublishingSdkVersion()
        {
            var publishingSdkVer = Resources.Load<TextAsset>("PublishingSdkVersion");
            if (null == publishingSdkVer || String.IsNullOrEmpty(publishingSdkVer.text))
            {
                return "none";
            }

            return publishingSdkVer.text.Trim();
        }

        public void SetRegulationData(RegulationSource source, RegulationType type, long version, string locale)
        {
            if (defaultParams.ContainsKey("regulation_source"))
            {
                defaultParams["regulation_source"] = source.ToString().ToLower();
            }
            else
            {
                defaultParams.Add("regulation_source", source.ToString().ToLower());
            }

            if (defaultParams.ContainsKey("regulation_type"))
            {
                defaultParams["regulation_type"] = type.ToString().ToLower();
            }
            else
            {
                defaultParams.Add("regulation_type", type.ToString().ToLower());
            }

            if (defaultParams.ContainsKey("regulation_version"))
            {
                defaultParams["regulation_version"] = version.ToString();
            }
            else
            {
                defaultParams.Add("regulation_version", version.ToString());
            }

            if (defaultParams.ContainsKey("consent_locale"))
            {
                defaultParams["consent_locale"] = locale.ToLower();
            }
            else
            {
                defaultParams.Add("consent_locale", locale.ToLower());
            }
        }

        public void CheckSession()
        {
            lock (locker)
            {
                int currentTime = Utils.CurrentTimeSeconds();

                if (currentTime - mLastEventTime >= sessionTimeoutDuration)
                {
                    NewSession();
                }
            }
        }

        void NewSession()
        {
            mSessionId = Guid.NewGuid().ToString();
            mLastEventTime = Utils.CurrentTimeSeconds();
            defaultParams["session_id"] = mSessionId;
            if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] NewSession: {mSessionId}");
        }

        void LoadSessionInfo()
        {
            lock (locker)
            {
                mSessionId = SessionId;
                mLastEventTime = LastEventTime;
                defaultParams.Add("session_id", mSessionId);
            }
        }

        public void SaveSessionInfo()
        {
            lock (locker)
            {
                if (mLastEventTime == 0 || string.IsNullOrEmpty(mSessionId))
                {
                    return;
                }

                LastEventTime = mLastEventTime;
                SessionId = mSessionId;
            }
        }
    }
}
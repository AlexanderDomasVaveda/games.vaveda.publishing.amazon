// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Scripts.Analytics
{
    internal static class Utils
    {
        internal static int CurrentTimeSeconds()
        {
            DateTime epochStart = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            int currentEpochTime = (int)(DateTime.UtcNow - epochStart).TotalSeconds;

            return currentEpochTime;
        }

        internal static string GetOsName()
        {
            return Application.platform == RuntimePlatform.IPhonePlayer ? "ios" :
                Application.platform == RuntimePlatform.Android ? Defines.IsAmazon ? "amazon" : "android" :
                Application.platform.ToString().ToLower();
        }

        internal static string GetDeviceType()
        {
#if UNITY_ANDROID
            var aspectRatio = (float)Math.Max(Screen.width, Screen.height) / Math.Min(Screen.width, Screen.height);
            var isTablet = (DeviceDiagonalSizeInInches() > 6.5f && aspectRatio < 2f);
            return isTablet ? "tablet" : "phone";
#elif UNITY_IOS
            bool deviceIsIpad = UnityEngine.iOS.Device.generation.ToString().Contains("iPad");
            if (deviceIsIpad)
            {
                return "tablet";
            }

            bool deviceIsIphone = UnityEngine.iOS.Device.generation.ToString().Contains("iPhone");
            if (deviceIsIphone)
            {
                return "phone";
            }

            return "Unknown";
#else
            return string.Empty;
#endif
        }

        internal static string GetInstallTime()
        {
#if UNITY_ANDROID
            try
            {
                var contextCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var context = contextCls.GetStatic<AndroidJavaObject>("currentActivity");
                var packageMngr = context.Call<AndroidJavaObject>("getPackageManager");
                var packageName = context.Call<string>("getPackageName");
                var packageInfo = packageMngr.Call<AndroidJavaObject>("getPackageInfo", packageName, 0);
                return (packageInfo.Get<long>("firstInstallTime") / 1000).ToString();
            }
            catch
            {
                return CurrentTimeSeconds().ToString();
            }
#endif
            return CurrentTimeSeconds().ToString();
        }

        internal static string GetOsVersion()
        {
#if UNITY_ANDROID
            using (var version = new AndroidJavaClass("android.os.Build$VERSION"))
            {
                try
                {
                    var releaseVersion = version.GetStatic<string>("RELEASE");
                    return releaseVersion;
                }
                catch (Exception e)
                {
                    return SystemInfo.operatingSystem;
                }
            }
#elif UNITY_IOS
            return UnityEngine.iOS.Device.systemVersion;
#else
            return SystemInfo.operatingSystem;
#endif
        }

        internal static string GetAltId()
        {
#if UNITY_ANDROID
            try
            {
                var contextCls = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
                var context = contextCls.GetStatic<AndroidJavaObject>("currentActivity");
                var contentResolver = context.Call<AndroidJavaObject>("getContentResolver");
                using (var secure = new AndroidJavaClass("android.provider.Settings$Secure"))
                {
                    return secure.CallStatic<string>("getString", contentResolver, "android_id");
                }
            }
            catch (Exception e)
            {
                return "0";
            }
#elif UNITY_IOS
            return UnityEngine.iOS.Device.vendorIdentifier;
#else
            return string.Empty;
#endif
        }

        private static float DeviceDiagonalSizeInInches()
        {
            var screenWidth = Screen.width / Screen.dpi;
            var screenHeight = Screen.height / Screen.dpi;
            var diagonalInches = Mathf.Sqrt(Mathf.Pow(screenWidth, 2) + Mathf.Pow(screenHeight, 2));

            return diagonalInches;
        }
    }
}
// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FAdsSdk;
using UnityEngine;
using Vaveda.Integration.Scripts.AdjustServices;
using Vaveda.Integration.Scripts.Analytics.Builders;
using Vaveda.Integration.Scripts.FacebookServices;
using Vaveda.Integration.Scripts.FirebaseServices;
using Vaveda.Integration.Scripts.Interfaces;
using Vaveda.Integration.Scripts.Regulation;
using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Scripts.Analytics
{
    public class AnalyticsServicesContainer : IAnalyticsExtended
    {
        private readonly DefaultMessagesService defaultMessagesService;
        private readonly List<IAnalyticsCommon> analyticsCommons;

        private readonly Dictionary<string, Dictionary<string, string[]>> filter;

        private TaskCompletionSource<bool> initializationTask;

        public EventDestination EventDestination => EventDestination.None;

        public AnalyticsServicesContainer(string analyticsEventFilterJson, AnalyticsBuilder analyticsBuilder)
        {
            initializationTask = new TaskCompletionSource<bool>();

            defaultMessagesService = new DefaultMessagesService();
            filter = JsonUtility.FromJson<AnalyticsFilter>(analyticsEventFilterJson).ToDictionary();

            if (Defines.IsEditor)
            {
                analyticsCommons = new List<IAnalyticsCommon>();
                return;
            }

            analyticsCommons = new List<IAnalyticsCommon>();

            InitializeServices(analyticsBuilder);
        }

        private async void InitializeServices(AnalyticsBuilder analyticsBuilder)
        {
            var result = await defaultMessagesService.InitializeDefaultMessagesService();
            if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] Default params initialized. Exist advt_id: {result}");

            analyticsCommons.Add(new VavedaAnalyticsService(defaultMessagesService));
            analyticsCommons.Add(new FacebookService());
            analyticsCommons.Add(new FirebaseService());
            analyticsCommons.Add(new AdjustAnalyticsService());

            for (int i = analyticsCommons.Count - 1; i >= 0; i--)
            {
                var analyticType = analyticsCommons[i].GetType();

                if (!analyticsBuilder.Types.Contains(analyticType))
                {
                    analyticsCommons.Remove(analyticsCommons[i]);
                }
            }

            initializationTask.SetResult(true);
        }

        public async Task Initialize()
        {
            await initializationTask.Task;
            if (Defines.IsLogEnabled)
                Debug.Log($"[FAds Vaveda] AnalyticsServicesContainer count {analyticsCommons.Count}");
            await Task.WhenAll(analyticsCommons.Select(x => x.Initialize()));
        }

        public void TutorialEndEvent(string levelName, int levelIndex, int levelTime, int levelResult,
            int levelScore = 0,
            int totalScore = 0)
        {
            analyticsCommons.ForEach(x => x.TutorialEndEvent(
                levelName, levelIndex, levelTime,
                levelResult, levelScore, totalScore));
        }

        public void ConsentEvent()
        {
            analyticsCommons.ForEach(x => x.ConsentEvent());
        }

        public void LevelStartEvent(string levelName, int levelPosition, int levelScore = 0,
            int totalScore = 0, string levelData = null)
        {
            analyticsCommons.ForEach(x =>
                x.LevelStartEvent(levelName, levelPosition, levelScore, totalScore, levelData));
        }

        public void LevelEndEvent(string levelName, int levelPosition, int levelTime, int levelResult,
            int levelScore = 0, int totalScore = 0, string levelData = null)
        {
            analyticsCommons.ForEach(x =>
                x.LevelEndEvent(levelName, levelPosition, levelTime, levelResult, levelScore, totalScore, levelData));
        }

        public void LevelTrackEvent(string levelName, int levelPosition, int levelTime,
            int levelResult, int levelScore, int totalScore,
            int levelFlowVal, int levelFlowMax, string levelData = null)
        {
            analyticsCommons.ForEach(x =>
                x.LevelTrackEvent(levelName, levelPosition, levelTime,
                    levelResult, levelScore, totalScore,
                    levelFlowVal, levelFlowMax, levelData));
        }

        public void ChooseEvent(string levelName, int levelPosition, string chooseType, string chooseOption,
            int chooseResult)
        {
            analyticsCommons.ForEach(x =>
                x.ChooseEvent(levelName, levelPosition, chooseType, chooseOption, chooseResult));
        }

        public void GetItemEvent(string placement, string levelName, int levelPosition, string itemType,
            string itemName,
            int itemQuantity = 1)
        {
            analyticsCommons.ForEach(x =>
                x.GetItemEvent(placement, levelName, levelPosition, itemType, itemName, itemQuantity));
        }

        public void SendEvent(string eventName, Dictionary<string, string> data)
        {
            OnAdsEvent(new FAdsEvent(eventName, data, EventAnalyticDestination.Everywhere));
        }

        public void OnAdsEvent(FAdsEvent fAdsEvent)
        {
            var parameters = fAdsEvent.parameters == null
                ? new Dictionary<string, string>()
                : new Dictionary<string, string>(fAdsEvent.parameters);

            analyticsCommons.ForEach(x =>
            {
                fAdsEvent.parameters = FilteredData(x.EventDestination, fAdsEvent.name, parameters);

                if (fAdsEvent.parameters != null)
                {
                    x.OnAdsEvent(fAdsEvent);
                }
            });
        }

        private Dictionary<string, string> FilteredData(EventDestination dest, string eventName,
            IDictionary<string, string> data)
        {
            if (dest == EventDestination.None)
            {
                return null;
            }

            var filterKey = dest.ToString().ToLower();

            if (filter.ContainsKey(filterKey))
            {
                return filter[filterKey].ContainsKey(eventName)
                    ? (filter[filterKey][eventName].Length == 1 && filter[filterKey][eventName][0].Equals("*"))
                        ? new Dictionary<string, string>(data)
                        : filter[filterKey][eventName].Where(data.ContainsKey)
                            .ToDictionary(key => key, key => data[key])
                    : null;
            }

            return new Dictionary<string, string>(data);
        }

        public void SetEnable()
        {
            analyticsCommons
                .OfType<IAnalyticsExtended>()
                .ToList().ForEach(x => x.SetEnable());
        }

        public Dictionary<string, string> GetAdsPostRequestParams()
        {
            var postParams = new Dictionary<string, string>
            {
                {"os_name", defaultMessagesService.DefaultParams["os_name"]},
                {"os_version", defaultMessagesService.DefaultParams["os_version"]},
                {"api_version", defaultMessagesService.DefaultParams["api_version"]},
                {"device_type", defaultMessagesService.DefaultParams["device_type"]},
                {"package_name", defaultMessagesService.DefaultParams["package_name"]},
                {"sdk_version", defaultMessagesService.DefaultParams["sdk_version"]}
            };

            if (defaultMessagesService.DefaultParams.ContainsKey("ad_id"))
            {
                postParams.Add("ad_id", defaultMessagesService.DefaultParams["ad_id"]);
            }

            return postParams;
        }

        public void SetRegulationData(RegulationSource source, RegulationType type, long version, string locale)
        {
            defaultMessagesService?.SetRegulationData(source, type, version, locale);
        }

        public void SaveSession()
        {
            analyticsCommons
                .OfType<IAnalyticsExtended>()
                .ToList().ForEach(x => x.SaveSession());
        }

        public void ShowConsentEvent(RegulationSource regulationSource, RegulationType regulationType,
            long regulationVersion, string locale)
        {
            analyticsCommons
                .OfType<IAnalyticsExtended>()
                .ToList()
                .ForEach(x => x.ShowConsentEvent(regulationSource, regulationType, regulationVersion, locale));
        }
    }
}
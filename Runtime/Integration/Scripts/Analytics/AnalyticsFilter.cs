using System;
using System.Collections.Generic;
using System.Linq;

namespace Vaveda.Integration.Scripts.Analytics
{
    public class AnalyticsFilter
    {
        public Destination[] Destinations = null;

        public Dictionary<string, Dictionary<string, string[]>> ToDictionary()
        {
            var result = new Dictionary<string, Dictionary<string, string[]>>();
            if (Destinations == null)
                return result;
            foreach (var dest in Destinations)
            {
                var events = dest.Events.Where(item => item.Key != null)
                    .ToDictionary(item => item.Key, item => item.Parameters);

                result.Add(dest.Name, events);
            }

            return result;
        }
    }

    [Serializable]
    public class Destination
    {
        public string Name;

        public Event[] Events;
    }

    [Serializable]
    public class Event
    {
        public string Key;
        public string[] Parameters;
    }
}
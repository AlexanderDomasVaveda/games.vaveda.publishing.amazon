namespace Vaveda.Integration.Scripts.Analytics
{
    public enum EventDestination
    {
        None,
        Firebase,
        Adjust,
        Vaveda
    }
}
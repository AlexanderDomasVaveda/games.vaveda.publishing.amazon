﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using JetBrains.Annotations;
using UnityEngine;

namespace Vaveda.Integration.Scripts.Utils
{
    /// <summary>
    /// Create if null
    /// DontDestroyOnLoad if haven't parent
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SingletonBehaviour<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected static T item;

        public static T Instance
        {
            get
            {
                if (item != null)
                {
                    return item;
                }

                item = FindObjectOfType<T>();

                if (item == null)
                {
                    item = new GameObject(typeof(T).ToString()).AddComponent<T>();
                }

                return item;
            }
        }

        [UsedImplicitly]
        protected void Awake()
        {
            if (Instance != null && Instance != this)
            {
                Destroy(gameObject);
            }

            if (Instance != null && Instance.transform.parent == null)
            {
                DontDestroyOnLoad(gameObject);
            }
        }
    }
}
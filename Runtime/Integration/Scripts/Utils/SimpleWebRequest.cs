// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Vaveda.Integration.Scripts.Utils
{
    public class SimpleWebRequest
    {
        private enum RequestType
        {
            Get,
            Post
        }

        private Uri Uri { get; }

        public SimpleWebRequest(Uri uri)
        {
            Uri = uri;
        }

        public async Task<string> Get()
        {
            return await Task.Run(() => SendRequest(RequestType.Get, null));
        }

        public async Task<string> Post(IEnumerable<KeyValuePair<string, string>> requestData)
        {
            return await Task.Run(() => SendRequest(RequestType.Post, requestData));
        }

        private async Task<string> SendRequest(RequestType requestType,
            IEnumerable<KeyValuePair<string, string>> requestData)
        {
            var client = new HttpClient {Timeout = TimeSpan.FromSeconds(60)};

            HttpResponseMessage response;

            switch (requestType)
            {
                case RequestType.Get:
                    response = await client.GetAsync(Uri);
                    break;
                case RequestType.Post:
                    var content = new FormUrlEncodedContent(requestData);

                    response = await client.PostAsync(Uri, content);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(requestType), requestType, null);
            }

            if (!response.IsSuccessStatusCode)
            {
                throw new Exception($"Response status: [{response.StatusCode}]");
            }

            return await response.Content.ReadAsStringAsync();
        }
    }
}
// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;

namespace Vaveda.Integration.Scripts.Utils
{
    public static class Defines
    {
        public static bool IsIos =>
#if UNITY_IOS
            true;
#else
            false;
#endif

        public static bool IsAndroid =>
#if UNITY_ANDROID
            true;
#else
            false;
#endif

        public static bool IsAmazon =>
#if UNITY_AMAZON
            true;
#else
            false;
#endif

        /// <summary>
        /// For Android. Call command and run app.
        /// <para>enable: adb shell setprop persist.log.tag.Vaveda VERBOSE</para>
        /// <para>disable: adb shell setprop persist.log.tag.Vaveda \"\""</para>
        /// </summary>
        public static bool IsLogEnabled =
#if LOG_ENABLED
            true;
#elif UNITY_ANDROID
            getAndroidLogStatus();
#else
            false;
#endif

        private static bool getAndroidLogStatus()
        {
            AndroidJavaClass log = new AndroidJavaClass("android.util.Log");

            return log.CallStatic<bool>("isLoggable", "Vaveda", 2);
        }

        public static bool IsEditor =>
#if UNITY_EDITOR
            true;
#else
            false;
#endif

        public static bool IsUnityCloud =>
#if UNITY_CLOUD_BUILD
            true;
#else
            false;
#endif
        public static bool IsUnityCloudMaster =>
#if UNITY_CLOUD_MASTER
            true;
#else
            false;
#endif
    }
}
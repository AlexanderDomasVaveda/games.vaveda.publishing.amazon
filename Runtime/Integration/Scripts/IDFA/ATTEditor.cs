namespace Vaveda.Integration.Scripts
{
    public class ATTEditor
    {
        public static int GetAuthorizationStatus() { return 0; }
        public static bool ShouldShowDialog() { return false; }
        public static void ShowDialog() { }
        public static void RegisterAppForAdNetworkAttribution() { }
        public static void ConversionEvent(int eventN) { }
        public static bool IsAvailableInSettings() { return false; }
        public static void OpenAppSettings() { }
    }
}
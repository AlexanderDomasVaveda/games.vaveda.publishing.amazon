using System.Runtime.InteropServices;

namespace Vaveda.Integration.Scripts
{
#if UNITY_IOS
    public class ATTiOS
    {
        /// <summary>
        /// The status values for app tracking authorization.
        /// The returned value is either:
        /// <para>0: notDetermined</para>
        /// <para>1: restricted</para>
        /// <para>2: denied</para>
        /// <para>3: authorized</para>
        /// </summary>
        public static int GetAuthorizationStatus() { return FATT_AuthorizationStatus(); }
        public static bool ShouldShowDialog() { return FATT_ShouldShow(); }
        public static void ShowDialog() { FATT_Show(); }
        public static void RegisterAppForAdNetworkAttribution() { FATT_RegisterApp(); }
        public static void ConversionEvent(int eventN) { FATT_ConversionEvent(eventN); }
        public static bool IsAvailableInSettings() { return FATT_IsAvailableInSettings(); }
        public static void OpenAppSettings() { FATT_OpenAppSettings(); }

        [DllImport("__Internal")]
        private static extern int FATT_AuthorizationStatus();

        [DllImport("__Internal")]
        private static extern bool FATT_ShouldShow();

        [DllImport("__Internal")]
        private static extern void FATT_Show();

        [DllImport("__Internal")]
        private static extern bool FATT_IsAvailableInSettings();

        [DllImport("__Internal")]
        private static extern void FATT_RegisterApp();

        [DllImport("__Internal")]
        private static extern void FATT_ConversionEvent(int eventN);

        [DllImport("__Internal")]
        private static extern void FATT_OpenAppSettings();
    }
#endif
}
﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Core.Common
{
    public class OpenUrl : MonoBehaviour, IPointerDownHandler
    {
        [SerializeField] private string url;

        public void OnPointerDown(PointerEventData eventData)
        {
            Application.OpenURL(url);
        }
    }
}
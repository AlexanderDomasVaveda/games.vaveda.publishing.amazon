﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.UI;

namespace Vaveda.Integration.Scripts.Regulation
{
    public class TermOfPolicyCanvas : MonoBehaviour
    {
        [SerializeField] private GameObject background;

        [SerializeField] private GameObject contentContainer;

        [SerializeField] private GameObject gdpr;

        [SerializeField] private GameObject ccpa;

        [SerializeField] private Button acceptButton;

        private ScreenOrientation currentOrientation;

        [UsedImplicitly]
        private void OnEnable()
        {
            currentOrientation = Screen.orientation;

            Screen.orientation = ScreenOrientation.Portrait;
        }

        [UsedImplicitly]
        private void OnDisable()
        {
            Screen.orientation = currentOrientation;
        }

        public async Task ShowGdpr()
        {
            await Show(true);
        }

        public async Task ShowCcpa()
        {
            await Show(false);
        }

        private async Task Show(bool isGdpr)
        {
            gameObject.SetActive(true);
            contentContainer.SetActive(true);
            background.SetActive(true);

            gdpr.SetActive(isGdpr);
            ccpa.SetActive(!isGdpr);

            var tcs = new TaskCompletionSource<object>();

            acceptButton.onClick.AddListener(() => tcs.SetResult(default));

            await tcs.Task;

            acceptButton.onClick.RemoveAllListeners();
            gameObject.SetActive(false);
        }
    }
}
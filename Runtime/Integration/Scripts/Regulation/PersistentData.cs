// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using UnityEngine;
using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Scripts.Regulation
{
    internal static class PersistentData
    {
        private const string DefaultKey = "[games.vaveda.fads]{0}";

        internal static class Privacy
        {
            public static long GdprVersion
            {
                get
                {
                    return long.Parse(PlayerPrefs.GetString(GetKey("gdprVersion"), "0"));
                }
                set
                {
                    PlayerPrefs.SetString(GetKey("gdprVersion"), value.ToString());
                    PlayerPrefs.Save();
                }
            }

            public static long CcpaVersion
            {
                get
                {
                    return long.Parse(PlayerPrefs.GetString(GetKey("ccpaVersion"), "0"));
                }
                set
                {
                    PlayerPrefs.SetString(GetKey("ccpaVersion"), value.ToString());
                    PlayerPrefs.Save();
                }
            }

            public static bool Optout
            {
                get => Convert.ToBoolean(PlayerPrefs.GetInt(GetKey("optout"), 0));
                set
                {
                    PlayerPrefs.GetInt(GetKey("optout"), Convert.ToInt32(value));
                    PlayerPrefs.Save();
                }
            }

            public static RegulationSource RegulationSource
            {
                get
                {
                    var source = PlayerPrefs.GetString(GetKey("regulationSource")).ToLower();
                    switch (source)
                    {
                        case "fads":
                            return RegulationSource.FAds;
                        case "api":
                            return RegulationSource.Api;
                        case "app":
                            return RegulationSource.App;
                        default:
                            return RegulationSource.Undefine;
                    }
                }
                set
                {
                    PlayerPrefs.SetString(GetKey("regulationSource"), value.ToString().ToLower());
                    PlayerPrefs.Save();
                }
            }

            public static RegulationType LastRegulationType
            {
                get
                {
                    var type = PlayerPrefs.GetString(GetKey("lastRegulationType")).ToLower();
                    switch (type)
                    {
                        case "gdpr":
                            return RegulationType.Gdpr;
                        case "ccpa":
                            return RegulationType.Ccpa;
                        default:
                            return RegulationType.Undefine;
                    }
                }
                set
                {
                    PlayerPrefs.SetString(GetKey("lastRegulationType"), value.ToString().ToLower());
                    PlayerPrefs.Save();
                }
            }


            public static string ConsentLocale
            {
                get => PlayerPrefs.GetString(GetKey("consentLocale"));
                set
                {
                    PlayerPrefs.SetString(GetKey("consentLocale"), value);
                    PlayerPrefs.Save();
                }
            }
        }

        internal static class Analytics
        {
            public static string UserId
            {
                get
                {
                    if (PlayerPrefs.HasKey(GetKey("userId")))
                    {
                        return PlayerPrefs.GetString(GetKey("userId"));
                    }

                    var userId = Guid.NewGuid().ToString();
                    PlayerPrefs.SetString(GetKey("userId"), userId);
                    PlayerPrefs.Save();

                    return userId;
                }
            }

            public static string InstallTime
            {
                get
                {
                    if (Defines.IsAndroid)
                    {
                        return Scripts.Analytics.Utils.GetInstallTime();
                    }

                    // For iOs and others devices only first run
                    if (PlayerPrefs.HasKey(GetKey("firstRun")))
                    {
                        return PlayerPrefs.GetString(GetKey("firstRun"));
                    }

                    var defaultValue = Scripts.Analytics.Utils.CurrentTimeSeconds().ToString();
                    PlayerPrefs.SetString(GetKey("firstRun"), defaultValue);
                    PlayerPrefs.Save();
                    return defaultValue;
                }
            }

            public static bool IsInstallEventSent
            {
                get => Convert.ToBoolean(PlayerPrefs.GetInt(GetKey("installEventSent"), 0));
                set
                {
                    PlayerPrefs.SetInt(GetKey("installEventSent"), Convert.ToInt32(value));
                    PlayerPrefs.Save();
                }
            }

            public static int LastEventTime
            {
                get
                {
                    return PlayerPrefs.GetInt(GetKey("sessionTime"), 0);
                }
                set
                {
                    PlayerPrefs.SetInt(GetKey("sessionTime"), value);
                    PlayerPrefs.Save();
                }
            }

            public static string SessionId
            {
                get
                {
                    return PlayerPrefs.GetString(GetKey("sessionId"), "");
                }
                set
                {
                    PlayerPrefs.SetString(GetKey("sessionId"), value);
                    PlayerPrefs.Save();
                }
            }
        }

        private static string GetKey(string key) =>
            string.Format(DefaultKey, key);
    }
}
﻿// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using UnityEngine;
using Vaveda.Integration.Scripts.Localization;
using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Scripts.Regulation
{
    public class RegulationService
    {
        private class RegulationApiResponse
        {
            [JsonProperty("regulation")] public string Regulation { get; set; }

            [JsonProperty("version")] public long Version { get; set; }
        }

        private readonly string regulationUrl = Defines.IsLogEnabled
            ? "https://api01.vaveda.games/test/regulation"
            : "https://us-central1-vaveda-api02.cloudfunctions.net/regulation";

        public bool IsRequestCompleted { get; private set; }

        internal long CurrentGdprVersion = PersistentData.Privacy.GdprVersion;
        internal long CurrentCcpaVersion = PersistentData.Privacy.CcpaVersion;
        internal RegulationSource CurrentRegulationSource = PersistentData.Privacy.RegulationSource;
        internal RegulationType CurrentLastRegulationType = PersistentData.Privacy.LastRegulationType;
        private string currentConsentLocale = PersistentData.Privacy.ConsentLocale;
        internal bool PersistentOptout { get; } = PersistentData.Privacy.Optout;

        internal bool IsCcpaApplies => CurrentCcpaVersion > 0;

        internal RegulationType ApiRegulationType = RegulationType.Error;
        internal long ApiRegulationVersion;

        internal RegulationService()
        {
            Task.Run(SendRequest);
        }

        internal void AcceptPrivacy(RegulationSource regulationSource, RegulationType regulationType, long version)
        {
            CurrentRegulationSource = PersistentData.Privacy.RegulationSource = regulationSource;
            CurrentLastRegulationType = PersistentData.Privacy.LastRegulationType = regulationType;

            if (regulationType == RegulationType.Gdpr)
            {
                CurrentGdprVersion =
                    PersistentData.Privacy.GdprVersion = version;
            }
            else if (regulationType == RegulationType.Ccpa)
            {
                CurrentCcpaVersion =
                    PersistentData.Privacy.CcpaVersion = version;
            }

            currentConsentLocale =
                PersistentData.Privacy.ConsentLocale = LocalizationService.Instance.CurrentLocale.Name;
        }

        internal (RegulationSource, RegulationType, long, string) GetAnalyticsData()
        {
            var version = 0L;

            if (CurrentLastRegulationType == RegulationType.Gdpr)
            {
                version = CurrentGdprVersion;
            }
            else if (CurrentLastRegulationType == RegulationType.Ccpa)
            {
                version = CurrentCcpaVersion;
            }

            return (CurrentRegulationSource, CurrentLastRegulationType, version, currentConsentLocale);
        }

        private async void SendRequest()
        {
            if (Defines.IsLogEnabled)
            {
                Debug.Log($"[FAds Vaveda] Start regulation request: {regulationUrl}");
            }

            RegulationApiResponse apiResponse = null;

            try
            {
                var url = new Uri(regulationUrl);
                var responseJson = await new SimpleWebRequest(url).Get();

                if (Defines.IsLogEnabled)
                {
                    Debug.Log($"[FAds Vaveda] Regulation response json:{responseJson}");
                }

                apiResponse = JsonConvert.DeserializeObject<RegulationApiResponse>(responseJson);

                if (Defines.IsLogEnabled)
                {
                    Debug.Log($"[FAds Vaveda] response: {apiResponse.Regulation} {apiResponse.Version}");
                }
            }
            finally
            {
                IsRequestCompleted = true;
                if (apiResponse != null)
                {
                    SetRegulationType(apiResponse.Regulation);
                    ApiRegulationVersion = apiResponse.Version;
                }
            }
        }

        private void SetRegulationType(string text)
        {
            switch (text.ToLower().Trim())
            {
                case "none":
                    ApiRegulationType = RegulationType.None;
                    break;
                case "ccpa":
                    ApiRegulationType = RegulationType.Ccpa;
                    break;
                case "gdpr":
                    ApiRegulationType = RegulationType.Gdpr;
                    break;
                default:
                    ApiRegulationType = RegulationType.Error;
                    break;
            }
        }
    }
}
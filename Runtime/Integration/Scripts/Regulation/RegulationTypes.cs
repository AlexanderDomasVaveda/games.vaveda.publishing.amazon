// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

namespace Vaveda.Integration.Scripts.Regulation
{
    public enum RegulationType
    {
        None,
        Gdpr,
        Ccpa,
        Error,
        Undefine
    }

    public enum RegulationSource
    {
        FAds,
        Api,
        App,
        Undefine
    }
}
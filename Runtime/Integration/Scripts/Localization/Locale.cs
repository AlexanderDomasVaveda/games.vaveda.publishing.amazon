// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using Newtonsoft.Json;

namespace Vaveda.Integration.Scripts.Localization
{
    public class Locale
    {
        private readonly Dictionary<string, string> localizedStrings;

        public string Name { get; }

        public Locale(string name, string json)
        {
            Name = name;

            localizedStrings = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);
        }

        public string GetLocalizedValue(string key)
        {
            return localizedStrings[key];
        }
    }
}
// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Vaveda.Integration.Scripts.Localization
{
    public class LocalizationService
    {
        private const string DefaultLocale = "english";

        private static LocalizationService instance = new LocalizationService();
        public static LocalizationService Instance => instance ?? (instance = new LocalizationService());

        private readonly Dictionary<string, Locale> locales = new Dictionary<string, Locale>();

        private Locale currentLocale;

        public Locale CurrentLocale
        {
            get
            {
                if (null != currentLocale)
                {
                    return currentLocale;
                }

                var systemLanguage = Application.systemLanguage.ToString().ToLower();

                locales.TryGetValue(systemLanguage, out currentLocale);

                if (null != currentLocale)
                {
                    return currentLocale;
                }

                var defaultLocale = locales.FirstOrDefault(x => x.Key == DefaultLocale).Value;

                return currentLocale = defaultLocale ?? locales.First().Value;
            }
        }

        private LocalizationService()
        {
            Initialize();
        }

        private void Initialize()
        {
            var localesAssets = Resources.LoadAll<TextAsset>("Locales");

            foreach (var localesAsset in localesAssets)
            {
                locales.Add(localesAsset.name, new Locale(localesAsset.name, localesAsset.text));
            }
        }

        public string Translate(string key) =>
            CurrentLocale?.GetLocalizedValue(key);
    }
}
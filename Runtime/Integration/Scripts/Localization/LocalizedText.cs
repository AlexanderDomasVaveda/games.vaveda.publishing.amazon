// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using UnityEngine;
using UnityEngine.UI;

namespace Vaveda.Integration.Scripts.Localization
{
    [RequireComponent(typeof(Text))]
    public class LocalizedText : MonoBehaviour
    {
        [SerializeField]
        private Text text;

        [SerializeField]
        private string localizationKey;

        private void Reset()
        {
            text = GetComponent<Text>();
        }

        private void OnValidate()
        {
            if (!text)
            {
                text = GetComponent<Text>();
            }
        }

        private void OnEnable()
        {
            text.text = LocalizationService.Instance.Translate(localizationKey);
        }
    }
}
// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using com.adjust.sdk;
using FAdsSdk;
using UnityEngine;
using Vaveda.Integration.Scripts.Analytics;
using Vaveda.Integration.Scripts.Interfaces;
using Vaveda.Integration.Scripts.Regulation;
using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Scripts.AdjustServices
{
    public class AdjustAnalyticsService : IAnalyticsExtended
    {
        private bool isEnabled;

        public EventDestination EventDestination => EventDestination.Adjust;

        private readonly Dictionary<string, string> adjustEvents = new Dictionary<string, string>();

        public Task Initialize()
        {
            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Adjust initialized (Fake)");

            return Task.CompletedTask;
        }

        public void TutorialEndEvent(string levelName, int levelIndex, int levelTime, int levelResult,
            int levelScore = 0,
            int totalScore = 0)
        {
            //Do nothing
        }

        public void ConsentEvent()
        {
            //Do nothing
        }

        public void LevelStartEvent(string levelName, int levelPosition, int levelScore = 0,
            int totalScore = 0, string levelData = null)
        {
            //Do nothing
        }

        public void LevelEndEvent(string levelName, int levelPosition, int levelTime, int levelResult,
            int levelScore, int totalScore, string levelData)
        {
            //Do nothing
        }

        public void LevelTrackEvent(
            string levelName, int levelPosition, int levelTime,
            int levelResult, int levelScore, int totalScore,
            int levelFlowVal, int levelFlowMax, string levelData)
        {
            //Do nothing
        }

        public void ChooseEvent(string levelName, int levelPosition, string chooseType, string chooseOption,
            int chooseResult)
        {
            //Do nothing
        }

        public void GetItemEvent(string placement, string levelName, int levelPosition, string itemType,
            string itemName,
            int itemQuantity = 1)
        {
            //Do nothing
        }

        public void OnAdsEvent(FAdsEvent fAdsEvent)
        {
            if (!isEnabled || adjustEvents.Count == 0 || !adjustEvents.ContainsKey(fAdsEvent.name))
            {
                return;
            }

            if (Defines.IsLogEnabled)
                Debug.Log(
                    $"[FAds Vaveda] Adjust OnAdsEvent " +
                    $"{fAdsEvent.name}({adjustEvents[fAdsEvent.name]}), params:{fAdsEvent.parameters.Count}");

            AdjustEvent adjustEvent = new AdjustEvent(adjustEvents[fAdsEvent.name]);

            foreach (KeyValuePair<string, string> keyValuePair in fAdsEvent.parameters)
            {
                adjustEvent.addCallbackParameter(keyValuePair.Key, keyValuePair.Value);
                if (Defines.IsLogEnabled)
                    Debug.Log($"[FAds Vaveda] Adjust param {keyValuePair.Key}='{keyValuePair.Value ?? "null"}'");
            }

            Adjust.trackEvent(adjustEvent);
        }

        public void SendEvent(string eventName, Dictionary<string, string> data)
        {
            //Do nothing
        }

        public void SetEnable()
        {
            if (isEnabled)
            {
                return;
            }

            AdjustConfig cfg = GetAdjustConfig();
            if (null == cfg)
            {
                return;
            }

            ReadAdjustEvents();

            Adjust.start(cfg);

            isEnabled = true;

            if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Adjust is enabled");
        }

        private static AdjustConfig GetAdjustConfig()
        {
            var adjustKeyFile = Resources.Load<TextAsset>("AdjustKeyRelease");
            if (null == adjustKeyFile)
            {
                if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] Adjust Error: not exist key");
                return null;
            }

            var appToken = adjustKeyFile.text.Trim();
            var environment = Defines.IsUnityCloudMaster ? AdjustEnvironment.Production : AdjustEnvironment.Sandbox;
            var config = new AdjustConfig(appToken, environment);

            config.setEventBufferingEnabled(false);
            config.setSendInBackground(false);
            config.setLaunchDeferredDeeplink(true);
            config.setLogLevel(Defines.IsLogEnabled ? AdjustLogLevel.Debug : AdjustLogLevel.Assert);

            return config;
        }

        private void ReadAdjustEvents()
        {
            var adjustEventsFile = Resources.Load<TextAsset>("AdjustEventsRelease");
            if (null == adjustEventsFile)
            {
                return;
            }

            AdjustEvents events = null;
            try
            {
                events = JsonUtility.FromJson<AdjustEvents>(adjustEventsFile.text);
            }
            catch (Exception e)
            {
                if (Defines.IsLogEnabled) Debug.LogWarning($"[FAds Vaveda] Error parse adjust events file {e.Message}");
            }

            if (events?.Events == null)
            {
                if (Defines.IsLogEnabled) Debug.LogWarning($"[FAds Vaveda] Adjust parse error: Events is empty");

                return;
            }

            foreach (Event item in events.Events)
            {
                if (null == item.name || null == item.token)
                {
                    if (Defines.IsLogEnabled)
                        Debug.LogWarning($"[FAds Vaveda] Adjust parse error: event is null: " +
                                         $"{item.name ?? "name"}/{item.token ?? "token"}");
                    continue;
                }

                adjustEvents.Add(item.name, item.token);
            }
        }

        public Dictionary<string, string> GetAdsPostRequestParams()
        {
            //Do nothing
            return new Dictionary<string, string>();
        }

        public void ShowConsentEvent(RegulationSource regulationSource, RegulationType regulationType,
            long regulationVersion,
            string locale)
        {
            //Do nothing
        }

        public void SetRegulationData(RegulationSource source, RegulationType type, long version, string locale)
        {
            //Do nothing
        }

        public void SaveSession()
        {
            //Do nothing
        }
    }

    public class AdjustEvents
    {
        public Event[] Events = null;
    }

    [Serializable]
    public class Event
    {
        public string name;
        public string token;
    }
}
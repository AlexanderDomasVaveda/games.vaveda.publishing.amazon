// Copyright (C) 2020 Vaveda Games (https://vaveda.games) - All Rights Reserved.
// Unauthorized copying of this file, via any medium is strictly prohibited.
// Proprietary and confidential.

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FAdsSdk;
using Firebase;
using Firebase.Analytics;
using UnityEngine;
using Vaveda.Integration.Scripts.Analytics;
using Vaveda.Integration.Scripts.Interfaces;
using Vaveda.Integration.Scripts.Regulation;
using Vaveda.Integration.Scripts.Utils;

namespace Vaveda.Integration.Scripts.FirebaseServices
{
    public class FirebaseService : IAnalyticsExtended
    {
        private bool isEnabled;
        private TaskCompletionSource<bool> initializationTask;

        public EventDestination EventDestination => EventDestination.Firebase;

        public void ConsentEvent()
        {
            //Do nothing
        }

        public async Task Initialize()
        {
            initializationTask = new TaskCompletionSource<bool>();
            var dependencyStatus = await FirebaseApp.CheckAndFixDependenciesAsync();

            if (dependencyStatus == DependencyStatus.Available)
            {
                FirebaseApp unused = FirebaseApp.DefaultInstance;

                if (Defines.IsLogEnabled) Debug.Log($"[FAds Vaveda] Firebase {dependencyStatus}");

                initializationTask.SetResult(true);
            }
            else
            {
                if (Defines.IsLogEnabled)
                    Debug.LogError($"[FAds Vaveda] Could not resolve all Firebase dependencies: {dependencyStatus}");

                initializationTask.SetResult(false);
            }
        }

        public void TutorialEndEvent(string levelName, int levelIndex, int levelTime,
            int levelResult, int levelScore = 0, int totalScore = 0)
        {
            //Do nothing
        }

        public async void SetEnable()
        {
            if (isEnabled)
            {
                return;
            }

            isEnabled = true;

            try
            {
                await initializationTask.Task;

                FirebaseAnalytics.SetAnalyticsCollectionEnabled(true);
                FirebaseAnalytics.SetUserId(PersistentData.Analytics.UserId);

                if (Defines.IsLogEnabled) Debug.Log("[FAds Vaveda] Firebase is enabled");
            }
            catch (Exception e)
            {
                if (Defines.IsLogEnabled)
                {
                    Debug.Log($"[FAds Vaveda] Firebase error {e.Message}");
                }
            }
        }

        public Dictionary<string, string> GetAdsPostRequestParams()
        {
            //Do nothing
            return new Dictionary<string, string>();
        }

        public void ShowConsentEvent(RegulationSource regulationSource, RegulationType regulationType,
            long regulationVersion,
            string locale)
        {
            //Do nothing
        }

        public void SetRegulationData(RegulationSource source, RegulationType type, long version, string locale)
        {
            //Do nothing
        }

        public void SaveSession()
        {
            //Do nothing
        }

        public void LevelStartEvent(string levelName, int levelPosition, int levelScore = 0,
            int totalScore = 0, string levelData = null)
        {
            //Do nothing
        }

        public void LevelEndEvent(string levelName, int levelPosition, int levelTime, int levelResult,
            int levelScore, int totalScore, string levelData)
        {
            FirebaseAnalytics.LogEvent(FirebaseAnalytics.EventLevelUp,
                new Parameter(FirebaseAnalytics.ParameterLevel, levelPosition),
                new Parameter(FirebaseAnalytics.ParameterCharacter, levelName));
        }

        public void LevelTrackEvent(string levelName, int levelPosition, int levelTime,
            int levelResult, int levelScore, int totalScore,
            int levelFlowVal, int levelFlowMax, string levelData)
        {
            //Do nothing
        }

        public void ChooseEvent(string levelName, int levelPosition, string chooseType, string chooseOption,
            int chooseResult)
        {
            //Do nothing
        }

        public void GetItemEvent(string placement, string levelName, int levelPosition, string itemType,
            string itemName,
            int itemQuantity = 1)
        {
            //Do nothing
        }

        public void OnAdsEvent(FAdsEvent fAdsEvent)
        {
            if (fAdsEvent.parameters == null)
            {
                return;
            }

            List<Parameter> parameters = new List<Parameter>();
            parameters.AddRange(fAdsEvent.parameters.Select(item => new Parameter(item.Key, item.Value)));
            FirebaseAnalytics.LogEvent(fAdsEvent.name, parameters.ToArray());
        }

        public void SendEvent(string eventName, Dictionary<string, string> data)
        {
            OnAdsEvent(new FAdsEvent(eventName, data, EventAnalyticDestination.Everywhere));
        }
    }
}